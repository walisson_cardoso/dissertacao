
%% bare_conf.tex
%% V1.3
%% 2007/01/11
%% by Michael Shell
%% See:
%% http://www.michaelshell.org/
%% for current contact information.
%%
%% This is a skeleton file demonstrating the use of IEEEtran.cls
%% (requires IEEEtran.cls version 1.7 or later) with an IEEE conference paper.
%%
%% Support sites:
%% http://www.michaelshell.org/tex/ieeetran/
%% http://www.ctan.org/tex-archive/macros/latex/contrib/IEEEtran/
%% and
%% http://www.ieee.org/

%%*************************************************************************
%% Legal Notice:
%% This code is offered as-is without any warranty either expressed or
%% implied; without even the implied warranty of MERCHANTABILITY or
%% FITNESS FOR A PARTICULAR PURPOSE! 
%% User assumes all risk.
%% In no event shall IEEE or any contributor to this code be liable for
%% any damages or losses, including, but not limited to, incidental,
%% consequential, or any other damages, resulting from the use or misuse
%% of any information contained here.
%%
%% All comments are the opinions of their respective authors and are not
%% necessarily endorsed by the IEEE.
%%
%% This work is distributed under the LaTeX Project Public License (LPPL)
%% ( http://www.latex-project.org/ ) version 1.3, and may be freely used,
%% distributed and modified. A copy of the LPPL, version 1.3, is included
%% in the base LaTeX documentation of all distributions of LaTeX released
%% 2003/12/01 or later.
%% Retain all contribution notices and credits.
%% ** Modified files should be clearly indicated as such, including  **
%% ** renaming them and changing author support contact information. **
%%
%% File list of work: IEEEtran.cls, IEEEtran_HOWTO.pdf, bare_adv.tex,
%%                    bare_conf.tex, bare_jrnl.tex, bare_jrnl_compsoc.tex
%%*************************************************************************

\documentclass[10pt, conference, compsocconf]{IEEEtran}

\usepackage{color}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{subfig}
\usepackage{array}
\usepackage[brazilian]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{caption}
\usepackage{graphicx}
\usepackage{mathtools}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}
\usepackage[outdir=./images/]{epstopdf}


% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}

\title{ABC: uma análise do aumento do número de dimensões modificadas na equação de busca}

\author{\IEEEauthorblockN{Walisson Gomes}
\IEEEauthorblockA{Instituto de Ciências Exatas e Naturais\\
Universidade Federal do Pará\\
Bel\'em, Par\'a\\
Email: walisson.gomes@icen.ufpa.br}
\and
\IEEEauthorblockN{Claudomiro Sales}
\IEEEauthorblockA{Instituto de Ciências Exatas e Naturais\\
	Universidade Federal do Pará\\
Bel\'em, Par\'a\\
Email: cssj@ufpa.br}
}

\maketitle


\begin{abstract}
	Artificial Bee Colony algorithm is a swarm intelligence based technique that has shown a satisfactory performance for complex optimization problems even when compared with other well known techniques. Several approaches were already proposed in the literature to increase the performance of the algorithm as a general purpose optimization technique. Between the proposed modifications, there is a proposal of increasing the number of dimensions to alter in the candidate solutions per iteration. Although the modifications are proposed in some papers, no real effort has been made to determine specifically what is the ideal value of dimensions in such procedure. Therefore, this paper analyses this question with a more in depth overview.
\end{abstract}

\begin{IEEEkeywords}
 Artificial bee colony; optimization; diversity control; search equation;

\end{IEEEkeywords}


% For peer review papers, you can put extra information on the cover
% page as needed:
% \ifCLASSOPTIONpeerreview
% \begin{center} \bfseries EDICS Category: 3-BBND \end{center}
% \fi
%
% For peerreview papers, this IEEEtran command inserts a page break and
% creates the second title. It will be ignored for other modes.
\IEEEpeerreviewmaketitle


\section{Introduction}
\label{sec:introduction}

A natureza é uma fonte importante de inspiração para o desenvolvimento de diferentes algoritmos para a resolução de problemas complexos de otimização. Alguns dos exemplos mais populares são Algoritmos Genéticos, uma abordagem evolucionária baseada na sobrevivência do mais apto \cite{Goldberg1989}, e Enxame de Partículas, uma técnica de inteligência de enxame (\textit{Swarm Intelligence} -- SI) baseada no comportamento coletivo de enxames de animais como pássaros e peixes \cite{Kennedy1995}. Uma técnica de SI mais recente para a resolução de problemas multi-modais complexos é chamada Colônia Artificial de Abelhas (\textit{Artificial Bee Colony} -- ABC), que toma inspiração do comportamento coletivo de abelhas de mel na busca por fontes de alimento rentáveis.

O ABC foi primeiramente descrito por Karaboga em \cite{Karaboga2005}, e apresenta uma boa performance quando comparado a outras bem-conhecidas técnicas de otimização como Algoritmo Genético, Enxame de Partículas, Evolução Diferencial e Estratégia de Evolução \cite{Karaboga2014,Karaboga2009}. Alguns dos aspectos que fazem o ABC uma alternativa popular são facilidade de implementação, eficiência e número reduzido de parâmetros de configuração, sendo aplicado a variados problemas do mundo real \cite{Karaboga2014,Kaswan2015}. Uma das principais características do algoritmo é o uso de apenas uma equação de busca para guiar o movimento de todos os indivíduos da população. Essa equação causa a perturbação em uma das soluções atuais resultando na translação em uma das dimensões da solução à cada vez que é aplicada. A nova posição da solução será considerada apenas se a modificação resultaria em um melhoramento do \textit{fitness}.

Este trabalho traz uma investigação sobre as consequências na qualidades das soluções retornadas pelo algoritmo ao alterarmos a quantidade de dimensões $d$ na equação de busca. Para tanto, na Seção \ref{sec:trab_relacionados} trazemos outros trabalhos alinhados ao tópico aqui descrito, Seção \ref{sec:abc} traz as principais características do algoritmo, a Seção \ref{sec:eqc_busca} explora em detalhes o funcionamento da equação de busca e as consequências do aumento do número de dimensões, a Seção \ref{sec:exper_analises} traz uma bateria de testes para pôr à prova os elementos descritos e o trabalho se encerra com as conclusões e comentários finais na Seção \ref{sec:conclusao}.

\section{Trabalhos Relacionados}
\label{sec:trab_relacionados}

Apesar do bom desempenho, o ABC, como qualquer outra meta-heurística, apresenta algumas fraquezas que podem ser trabalhadas para o desenvolvimento de um algoritmo mais robusto. Por exemplo, autores argumentam que o algoritmo pode apresentar baixa velocidade de convergência em alguns casos \cite{Akay2012}, ficar preso em ótimos locais com certa frequência \cite{Xiang2017} e não ter uma capacidade de intensificação tão boa quanto a de exploração \cite{Zhu2010}.

Diferentes abordagens já foram testadas objetivando a melhora do algoritmo como otimizador genérico. Entre essas modificações, podem ser encontradas estratégias que emprestam mecanismos de outros algoritmos, como em \cite{Zhu2010,Liu2017}, onde os autores importam estruturas de \textit{global best} do Enxame de Partículas. Em  \cite{Kiran2015} os autores criam uma regra de decisão simples para tentar melhorar a convergência do algoritmo em alguns tipos de funções, movendo as abelhas da população para direções que se acredita serem mais promissoras. Em \cite{Gomes2018} os autores modificam a forma como as abelhas se movem, tornando a busca especialmente agressiva no começo do processo de otimização. No entanto, esta estratégia cria um \textit{drawback} na intensificação, muitas vezes resultando em convergência prematura e, portanto, sendo pouco indicada para problemas onde o número de iterações não é um fator limitante.

Existem dois trabalhos que perfeitamente se alinham ao tema aqui tratado, explorando o significado da alteração do número de dimensões modificadas simultaneamente na equação de busca da técnica, sendo os mais relevantes \cite{Akay2012} e \cite{Xiang2017}. Akay \cite{Akay2012} propõe a criação de um parâmetro $MR$ para controlar o número $d$ de dimensões que serão trabalhadas na equação de busca. $MR$ indiretamente determina o número de dimensões por um fator probabilístico. Suponha que $MR=0.1$, isso significa que por volta de 10\% do número de dimensões será utilizado, em contraponto ao algoritmo original, onde apenas uma dimensão é modificada por execução da equação de busca. O problema com essa estratégia é bastante evidente, uma vez que $MR$ provavelmente deve ser re-configurado para cada problema diferente. A abordagem pode se tornar desnecessariamente exaustiva para problemas desconhecidos e complexos.

Dada a problemática, \cite{Xiang2017} propõe a retirada do parâmetro $MR$ fixando o número de dimensões trabalhadas como $d=\sqrt{D}$, ou de forma mais prática, $d=\floor{\sqrt{D}}$, onde $D$ é a dimensionalidade do problema. Os autores argumentam que, embora a inserção do parâmetro $MR$ possa ser eficiente, aumenta a complexidade de configuração do algoritmo, mesmo que uma das vantagens do ABC seja justamente sua facilidade de uso. Na seção de resultados, poremos à prova esta afirmação, verificando se o valor sugerido de $d$ é realmente eficaz para a maior parte dos problemas.

A primeira proposta insere dificuldade de parametrização, embora seja mais flexível. A segunda proposta propõe um valor teoricamente aceitável para as diferentes classes de problemas, mesmo que neste trabalho não tenha sido desenvolvido um esforço de demonstração \textit{ad hoc}. Ambos os trabalhos também apresentam cada um uma modificação diferente no ABC, podendo mascarar a real significância das modificações do número de dimensões $d$ da técnica. Este artigo se propõe a investigar esta questão mais a fundo, analisando empiricamente se a segunda proposta é valida ou não, e, caso não, verificar se existe um valor padrão semelhante mais adequado.

\section{Colônia Artificial de Abelhas}
\label{sec:abc}

O ABC é uma técnica iterativa utilizada para minimizar uma função $f$ usando a troca de informações entre os membros da população. A população é dividida entre três tipos de abelhas: empregadas, espectadoras e escoteiras. A primeira metade da população é composta por abelhas empregadas que estão associadas cada uma a uma fonte de alimentação (uma solução candidata para o problema). A segunda metade é composta por abelhas espectadoras que irão visitar as fontes de alimento proporcionalmente às suas quantidades de néctar determinadas pelas abelhas empregadas, aprimorando o processo de intensificação. Quando uma fonte de alimento é exausta pelas abelhas, isto é, nenhum melhoramento acontece depois de uma quantidade pre-determinada $m$ de tentativas, a abelha empregada vira uma escoteira e vai à procura de uma nova fonte de alimento aleatória.

O ABC terá três parâmetros de configuração: o tamanho do enxame ($NP$), um número máximo $m$ de visitas sem sucesso ($limit$) e o número de ciclos do algoritmo ($maxCycle$). O algoritmos pode ser descrito pelos passos \cite{Karaboga2005}:

\begin{enumerate}
	\item Enviar as abelhas escoteiras para as fontes de alimentos iniciais aleatoriamente. A abelha escoteira $i$ se transforma em uma abelha empregada na posição $X_i$, que é um vetor de $D$ dimensões, representando uma solução candidata.
	\item Enviar as abelhas empregadas nas fontes de alimento e avaliar a qualidade de suas soluções dada a função de avaliação $f$.
	\item Calcular a probabilidade no qual abelhas espectadores irão visitar a fonte de alimento $i$. A probabilidade de visitar a fonte $i$ é dada por $p_i = \mathcal{F}(i)/\sum_{j=1}^{SN}\mathcal{F}(j)$, onde $SN$ é o número de abelhas empregadas, $\mathcal{F}(i)$ é a função $fitness$ como definido pela equação \ref{eq:fit}.
	\item Enviar as abelhas espectadoras seguindo um esquema de seleção proporcional e determinar a qualidade de suas soluções.
	\item Enviar as abelhas escoteiras para descobrir novas fontes de alimento.
	\item Memorizar a melhor fonte de alimento encontrada até o momento e voltar para o passo 2, se as condições ainda não foram cumpridas.
\end{enumerate}

\begin{equation}
\label{eq:fit}
\mathcal{F}(i) =
\begin{cases}
\frac{1}{1+f(i)} & f(i) \ge 0 \\ 
1+abs(f(i))      & f(i) < 0
\end{cases}
\end{equation}

Cada visita nos passos 2 e 4 consiste em uma tentativa de melhorar a posição de uma fonte de alimento baseada na equação \ref{eq:pos_update}. A posição da $i$-ésima abelha empregada é atualizada usando a informação de uma abelha aleatória de índice $k$, onde $j$ é uma dimensão escolhida ao acaso, $\phi$ é um número aleatório uniformemente distribuído no intervalo $[-1,+1]$, e $k\ne~i$. Se a solução é melhorada, uma abordagem gulosa é empregada, definindo $X_i'$ como a nova posição da fonte de alimento. Caso contrário, a nova posição é refutada e a fonte de alimentação permanece inalterada.

\begin{equation}
\label{eq:pos_update}
X_{i,j}' = X_{i,j} + \phi~(X_{i,j}-X_{k,j})
\end{equation}

Quando não ocorrem melhorias numa certa fonte de alimento depois de $limit$ visitas sem sucesso, a abelha empregada associada vira uma abelha escoteira e vai para uma posição aleatória do espaço de busca. Dessa forma, a abelha escoteira vai desempenhar o processo de exploração do espaço, abandonando fontes exaustas e adicionado áreas não exploradas ainda. Para manter um melhor controle da exploração, no máximo uma \textit{scout} é permitida por iteração.

\section{Equação de Busca}
\label{sec:eqc_busca}

Pela Equação \ref{eq:pos_update}, a posição da $i$-ésima abelha empregada pode ser modificada em uma dimensão uniformemente em ambas direções positiva e negativa. O tamanho do passo vai depender apenas da distância para a abelha selecionada aleatoriamente com índice $k$, como ilustrado na Figura \ref{fig:bee1}, que representa uma dimensão selecionada no espaço e a possibilidade de movimento é dada pela marca verde ilustrada na figura.

\begin{figure}[!t]
	\centering
	\includegraphics[trim={0 1cm 0 1cm},clip,width=0.48\textwidth]{images/case01a.eps}
	\caption{Ilustração da liberdade de movimento de uma abelha $b_i$ no ABC original.}
	\label{fig:bee1}
\end{figure}

A cada iteração, a equação de busca é executada exatamente $NP$ vezes. Na fase das abelhas empregadas, a equação é utilizada $SN$ vezes, uma para cada solução associada. Na fase das abelhas espectadoras, $SN$ execuções da equação serão proporcionalmente distribuídas entre as soluções correntes. Desta forma, a equação será utiliza no mínimo $1$ vez em cada fonte de alimento e no máximo $SN+1$ vezes. Ou de outra forma, pode-se afirmar que na versão básica do algoritmo, em média, a equação de busca será executada duas vezes para cada solução, por iteração.

Uma consequência interessante da simetria inerente à equação de busca é que não importa se o \textit{fitness} da abelha de referência é maior ou menor, a única métrica relevante na busca é a distância relativa entre as abelhas empregadas. Dessa forma, selecionar soluções de referência com maior ou menor \textit{fitness} não tem importância prática. Dito isto, é fácil perceber que a intensificação da busca vai ocorrer quando as abelhas estiverem reunidas próximas ao mínimo global, provavelmente nos últimos estágios do processo de otimização. Nos primeiros ciclos da otimização, a equação de busca vai desempenhar, inclusive, também um processo de exploração.

As críticas feitas na literatura sobre a velocidade de convergência do algoritmo às vezes falam sobre esta considerável lentidão para aprimorar soluções no espaço de busca. No entanto, faz-se necessário verificar se a crítica é realmente válida. De fato, para toda execução da equação, a posição de uma solução poderá ser modificada em no máximo uma dimensão, porém não necessariamente a convergência será lenta, uma vez que, pela fase das abelhas espectadoras, uma solução pode ter múltiplas dimensões modificadas por iteração. Como a melhor solução corrente tem maiores chances de ser selecionada, recebe maior ênfase no processo.

Considere-se então o aumento do número de dimensões na equação de busca dada por

\begin{equation}
\label{eq:pos_update_mult}
X_{i,J}' = X_{i,J} + \phi~(X_{i,J}-X_{k,J})
\end{equation}

onde J é um conjunto de $d$ números inteiros aleatórios distintos no intervalo $[1,SN]$. $\phi$ pode assumir duas formas diferentes que, apesar de sutis, causam uma grande discrepância no comportamento do algoritmo. Na primeira metodologia, $\phi$ é um conjunto de $d$ números aleatórios no intervalo $[-1,+1]$. Este esquema é semelhante ao utilizado em \cite{Akay2012,Xiang2017}. No segundo caso, $\phi$ é um único número que irá escalonar o deslocamento em todas as dimensões.

Na Figura \ref{fig:pos_mov} estão representadas uma função unimodal com duas dimensões, curvas de nível de formato elipsoide e o algoritmo sendo executado com apenas duas abelhas empregadas, representadas por dois pontos vermelhos. A equação de busca será executada uma vez na abelha inferior mais à esquerda em três situações diferentes. Na primeira situação a abelha de referência está situada sobre uma das curvas de nível e a outra está em um ponto que gera um melhor \textit{fitness}. Figuras \ref{fig:pos_mov}a, \ref{fig:pos_mov}d e \ref{fig:pos_mov}g, representam, respectivamente, as liberdades de movimento da abelha na equação de busca original (a), na equação de busca com duas dimensões e $\phi$ também bi-dimensional (d) e na equação de busca com duas dimensões e $\phi$ único para todas as dimensões (g). A mesma lógica segue para as demais situações, onde no segundo cenário a abelha de referência está tangente no ponto mais inferior e no terceiro cenário está tangente no ponto mais à esquerda.

\begin{figure*}
	\subfloat[]{\includegraphics[width=0.32\textwidth]{images/case01a.eps}}
	\hspace{0.02\textwidth}
	\subfloat[]{\includegraphics[width=0.32\textwidth]{images/case02a.eps}}
	\hspace{0.02\textwidth}
	\subfloat[]{\includegraphics[width=0.32\textwidth]{images/case03a.eps}}
	\vspace{1em}
	\subfloat[]{\includegraphics[width=0.32\textwidth]{images/case01b.eps}}
	\hspace{0.02\textwidth}
	\subfloat[]{\includegraphics[width=0.32\textwidth]{images/case02b.eps}}
	\hspace{0.02\textwidth}
	\subfloat[]{\includegraphics[width=0.32\textwidth]{images/case03b.eps}}
	\vspace{1em}
	\subfloat[]{\includegraphics[width=0.32\textwidth]{images/case01c.eps}}
	\hspace{0.02\textwidth}
	\subfloat[]{\includegraphics[width=0.32\textwidth]{images/case02c.eps}}
	\hspace{0.02\textwidth}
	\subfloat[]{\includegraphics[width=0.32\textwidth]{images/case03c.eps}}
	
	\caption{Possibilidade de deslocamento de uma abelha usando três metodologias diferentes em três cenários distintos. As metodologias são (a), (b) e (c) para o esquema do algoritmo original, (d), (e) e (f) para $\phi$ bi-dimensional e (g), (h) e (i), onde $\phi$ é o mesmo em ambas as dimensões.}
	\label{fig:pos_mov}
\end{figure*}

Baseado apenas no modelo original, não é possível determinar uma forma correta de escalonar o procedimento, mas podemos fazer algumas inferências sobre a probabilidade de uma solução melhorar estudando os três cenários representados na Figura \ref{fig:pos_mov}. As posições relativas entre as abelhas foram escolhidas para cobrir grande parte dos cenários encontrados pelo algoritmo quando tratando um ótimo local em uma situação real, considerando que outras diversas combinações poderiam ser derivadas destes cenários utilizando noções de simetria.

Pelo primeiro cenário, a probabilidade de a solução melhorar é $0<p(a)<=0.5$ para o caso (a) e $0<p(d)<=0.5$ para (d), dependendo da posição relativa das abelhas, e $p(g)=0.5$ para o caso (g). No segundo cenário, a probabilidade de melhora é $p(b)=0$ para o caso (b) e $0<p(e)<0.5$ para o caso (e) e $0<p(h)<0.5$ (h). No terceiro cenário, temos $0<p(c)<0.5$, $0<p(f)<0.5$ e $0<p(i)<0.5$, onde $p(c) >= p(f)$ e $p(c) >= p(i)$.

Os cenários criados indicam que cada forma da equação de busca apresenta vantagens e desvantagens, sendo difícil determinar qual seria a mais eficiente analisando visualmente o problema. A probabilidade de melhora da solução atual está situada no intervalo $[0,0.5]$ para qualquer situação. Dessa forma, foram gerados resultados utilizando as duas metodologias de generalização apresentadas, e a segunda ($\phi$ único para as várias dimensões) teve um desempenho levemente superior, achando um melhor resultado final na maioria das vezes. Logo, os resultados desta metodologia serão os apresentados neste trabalho.

Teoricamente, se o sucesso na atualização da posição de uma solução tiver a mesma probabilidade para os casos uni e bi-dimensionais, a convergência será mais rápida quando se aumentam o número de dimensões, uma vez que um passo com duas dimensões seria equivalente à dois passos utilizando uma dimensão. Existem dois pontos principais que podem contrapor esta conclusão:

\begin{enumerate}
	\item O aumento do número de dimensões reduz a probabilidade de sucesso em uma atualização de posição das soluções. Neste caso, o número de rejeições aumentaria mais rapidamente do que as vantagens oferecidas pelo deslocamento mais significativo.
	\item O aumento do número de dimensões causaria convergência prematura, onde as abelhas rapidamente ficariam presas à um ótimo local.
\end{enumerate}

Para mostrar se o aumento do número de dimensões resulta em convergência mais rápida e/ou melhora no resultado final do algoritmo, diversos experimentos foram realizados com funções de otimização clássicas da literatura.

\section{Experimentos e Análises}
\label{sec:exper_analises}

Para realização dos experimentos, o ABC foi implementado na linguagem de programação Python e os testes realizados em um computador com processador Intel$^{\textregistered}$ Core i7-4700MQ com velocidade de 2.5 GHz. 

No ABC implementado foram mantidos fixos alguns parâmetros para redução das variáveis do problema sendo analisado. O tamanho da população $NP$ foi fixado em 30, consequentemente, $SN=15$, e o parâmetro $limit$ foi configurado para $limit=SN*D$, como recomendado em \cite{Karaboga2005,Karaboga2008}.

\subsection{Funções de Teste}
\label{subsec:funcoes_teste}

Funções clássicas foram escolhidas para testar o ABC: Ackley ($f_1$), Alpine Nº1 ($f_2$), Griewank ($f_3$), Michalewicz ($f_4$), Rastrigin ($f_5$), Rosenbrock ($f_6$), Schwefel ($f_7$) e Sum of Squares ($f_8$). Todas as funções são dimensionalmente escaláveis e apenas a Michalewicz não tem seu mínimo em zero. Tabela \ref{tab:ranges} mostra as especificações dos problemas e os limites do espaço de busca usadas em todas as $D$ dimensões.

\begin{table}
	\centering
	\caption{Benchmark optimization problems and the range of search.}
	\label{tab:ranges}
	\begin{tabular}{lr}
		\hline
		\multicolumn{1}{c}{Objective Function} & Search Range \\
		\hline \\
		$f_1(x)=10D+\sum_{i=1}^{D}(x_i^2-10cos(2\pi x_i))$ & [-5.12, 5.12] \\ [2ex]
		$f_2(x)=\sum_{i=1}^{D}|x_i sin(x_i)+0.1x_i|$ & [-10, 10] \\ [2ex]
		$f_3(x)=20+e-20exp \left( -0.2\sqrt{\frac{1}{D}\sum_{i=1}^{D}x_i^2} \right) $  &  [-32.768, 32.768] \\[2ex]
		~~~~~~~~~~  $-exp(\frac{1}{D}\sum_{i=1}^{D}cos(2\pi x_i))$   & \\[2ex]
		$f_4(x)= -\sum_{i=1}^{D}sin(x_i)[sin(\frac{ix_i^2}{\pi}))]^{20}$  & [0, $\pi$] \\ [2ex]
		$f_5(x)=1+\frac{1}{4000}\sum_{i=1}^{D}x_i^2- \prod_{i=1}^{D}cos\left(\frac{x_i}{\sqrt{i}}\right)$  &  [-600, 600] \\[2ex]
		$f_6(x)=\sum_{i=1}^{D-1}\left(100(x_i^2-x_{i+1})^2+(1-x_i)^2\right)$  &  [-5, 10] \\[2ex]
		$f_7(x)=418.9829D+\sum_{i=1}^{D}-x_i~sin(\sqrt{\left|x_i\right|})$  &  [-500, 500] \\[2ex]
		$f_8(x)=\sum_{i=1}^{D}ix_i^2$  &  [-100, 100] \\[2ex]
		\hline
	\end{tabular}
\end{table}

\begin{figure*}[!h]
	\subfloat[\label{fig:10d_1000i}]{\includegraphics[width=0.35\textwidth]{images/10d_30pop_1000iter.eps}}
	\subfloat[\label{fig:10d_2000i}]{\includegraphics[trim={1cm 0 0 0},clip,width=0.32\textwidth]{images/10d_30pop_2000iter.eps}}
	\subfloat[\label{fig:10d_5000i}]{\includegraphics[trim={1cm 0 0 0},clip,width=0.32\textwidth]{images/10d_30pop_5000iter.eps}}
	
	\caption{Melhor valor encontrado para $f_{1-8}$ com $D=10$ e $maxCycle=1000$ (a), $2000$ (b) e $5000$ (c).}
	\label{fig:10d}
\end{figure*}

As funções têm diferentes peculiaridades que as fazem interessantes para otimização com meta-heurísticas, por exemplo, as sete primeiras são funções multi-modais, algumas têm grandes espaços de busca (Schwefel, Griewank) e funções com o mínimo global não localizado no centro do espaço (Rosenbrock, Schwefel, Michalewicz). Rastrigin e Schwefel têm muitos mínimos locais, fazendo com que sejam bons testes para a habilidade do algoritmo de escapar de soluções sub-ótimas. A função Rosenbrock tem um longo vale, especialmente difícil de convergir com técnicas baseadas em derivada e a função Griewank tem interdependência entre os seus termos. A \textit{Sum of Squares} é a única função unimodal do conjunto.

\subsection{Metodologia}
\label{subsec:metodologia}

Foram registradas as médias das respostas finais de dez execuções do algoritmo para todas as função do \textit{benchmark}. As funções foram otimizadas pelo ABC com $D$ igual a 10, 20 e 30 dimensões. Para cada caso, o algoritmo foi executado com $d=[1,2,...,D-1,D]$ dimensões na equação de busca. Dessa forma podemos acompanhar os efeitos do aumento da complexidade dos problemas e qual a configuração com melhor desempenho. Para verificar qual configuração se sobressaiu, iremos contar a frequência com a qual um certo $d$ retornou os melhores resultados. Por exemplo, se $d=2$ obteve o melhor resultado médio em cinco das oito funções, sua frequência é cinco, ou aqui também representada como (5/8).

Para casa diferente dimensionalidade dos problemas, os resultados foram gravados em três momentos distintos do processo de otimização, sendo $maxCycle=$ 1000, 2000 e 5000. Assim verificamos também se os comportamentos obtidos variam durante o decorrer do processo. 


\subsection{Resultado}
\label{subsec:resultado}


\begin{figure*}[!h]
	\subfloat[\label{fig:20d_1000i}]{\includegraphics[width=0.35\textwidth]{images/20d_30pop_1000iter.eps}}
	\subfloat[\label{fig:20d_2000i}]{\includegraphics[trim={1cm 0 0 0},clip,width=0.32\textwidth]{images/20d_30pop_2000iter.eps}}
	\subfloat[\label{fig:20d_5000i}]{\includegraphics[trim={1cm 0 0 0},clip,width=0.32\textwidth]{images/20d_30pop_5000iter.eps}}
	
	\caption{Melhor valor encontrado para $f_{1-8}$ com $D=10$ e$maxCycle=1000$ (a), $2000$ (b) e $5000$ (c).}
	\label{fig:20d}
\end{figure*}

A Figura \ref{fig:10d_1000i} mostra a média dos resultados finais para os problemas com dez dimensões e 1000 iterações. As pequenas marcações em vermelho indicam o número de dimensões onde foi encontrado o menor valor médio para as funções. Nestes gráficos, resultados cuja diferença é menor que $1e^{-30}$ são considerados iguais e resultados menores do que $1e^{-30}$ são considerados iguais a zero. Os melhores e piores resultados obtidos são exibidos à direita do gráfico. Pela Figura \ref{fig:10d_1000i} podemos notar que a maior frequência de número de dimensões foi obtida para $d=2$ (5/8). Como $\floor{\sqrt{10}}=2$, a suposição de Xiang \cite{Xiang2017} parece razoável para este primeiro exemplo.

Figura \ref{fig:10d_2000i} mostra os resultados quando são permitidas mais 1000 iterações do algoritmo. Como mostrado na figura, o cenário alterou-se razoavelmente, para $f_{4-8}$. $f_4$ e $f_5$ alcançaram as melhores respostas possíveis utilizando diferentes valores de $d$, porém é notável que a qualidade dos resultados se deteriora conforme continua-se aumentando $d$. Mesmo que $f_{1-3}$ continuem tendo melhores desempenhos para $d>1$, agora a maior frequência ocorre quando $d=1$ (5/8). Logo, $\floor{\sqrt{D}}$ deixou de ser a melhor estratégia quando aumentamos a quantidade de processamento permitida. Conclusões semelhantes podem ser tiradas quando o número de iterações igual a 5000, como ilustrado pela figura \ref{fig:10d_5000i}.

Figura \ref{fig:20d} mostra os resultados obtidos para $D=20$. Nesta caso podemos observar um comportamento um pouco mais estável nos resultados, onde o acréscimo no número de dimensões causou desempenhos consideravelmente menores em parte das funções. Para $maxCycle=1000$, temos novamente a maior frequência em $d=2$ (5/8), como anteriormente, inferior a $\floor{\sqrt{20}}=4$. Quando $maxCycle=2000$, a frequência de $d=2$ cai para (4/8) e de $d=1$ sobe para (3/8). Para $maxCycle=5000$, os melhores valores encontrados acabam por se distribuir em $d=1,2,3$ para seis das oito funções.

Finalmente, na Figura \ref{fig:30d} temos os resultados para $D=30$. Este valor torna as funções consideravelmente mais complexas, mas, mesmo neste caso, o ABC é capaz de encontrar valores bastante próximos ao ótimo global, mesmo com apenas 1000 iterações, como pode ser visto na Figura \ref{fig:30d_1000i}. A figura também revela que, assim como nos casos anteriores, os melhores resultados encontrados foram obtidos com valores baixos de $d$, sendo $d=2$ (4/8) e $d=3$ (3/8). Valores consideravelmente distantes de $\floor{\sqrt{30}}=5$. Quando $maxCycle=2000$, algo semelhante ocorreu, mas desta vez $d=3$ perdeu alguns pontos de frequência para $d=1$ (2/8). A frequência máxima de $d=1$ foi obtida para $maxCycle=5000$ (3/8), mesma de $d=2$.

\begin{figure*}[t]
	\subfloat[\label{fig:30d_1000i}]{\includegraphics[width=0.35\textwidth]{images/30d_30pop_1000iter.eps}}
	\subfloat[\label{fig:30d_2000i}]{\includegraphics[trim={1cm 0 0 0},clip,width=0.32\textwidth]{images/30d_30pop_2000iter.eps}}
	\subfloat[\label{fig:30d_5000i}]{\includegraphics[trim={1cm 0 0 0},clip,width=0.32\textwidth]{images/30d_30pop_5000iter.eps}}
	
	\caption{Melhor valor encontrado para $f_{1-8}$ com $D=10$ e $maxCycle=1000$ (a), $2000$ (b) e $5000$ (c).}
	\label{fig:30d}
\end{figure*}

Como pôde ser observado através dos experimentos, a afirmação de Xiang não parece válida para os três casos analisados, principalmente quando $maxCycle$ aumenta. Uma tendência observada com os diferentes estágios dos experimentos é que, quanto mais processamento é fornecido ao ABC, um menor valor de $d$ parece preferível para boa parte das funções testadas. Os experimentos indicam que com um aumento no número de dimensões alteradas simultaneamente na equação de busca, a procura inicial do algoritmo se torna mais agressiva e converge mais rapidamente. No entanto, o uso de $d=1$, valor original empregado, mostra-se mais estável e, com a devida quantidade de processamento fornecida, se torna a melhor opção para a maior parte dos casos.

Algumas funções realmente se beneficiaram do aumento de $d$, em especial a função Alpine N. 1. Dessa forma, a estratégia do uso de $MR$ proposta por Akay \cite{Akay2012} poderia trazer benefícios se um valor adequado fosse escolhido para o parâmetro, como $d=6$ ou $d=7$. Tal parâmetro em si é consideravelmente difícil de ser determinado sem conhecimento prévio da função a ser otimizada e, logo, seu uso é bastante restrito.


\section{Conclusão}
\label{sec:conclusao}

Este trabalho trouxe um esforço para uma melhor compreensão dos efeitos de se aumentar o número de dimensões alteradas simultaneamente na equação de busca do Algoritmo Colônia Artificial de Abelhas. Nele foram descritas formas de se generalizar o número $d$ de dimensões e experimentos revelaram se melhores resultados são obtidos comparativamente.

Como mostrado ao longo deste trabalho, o aumento do número de dimensões na equação de busca nem sempre traz melhores resultados para o processo de otimização do algoritmo. Alguns problemas se beneficiaram de tal modificação, mas na maior parte dos experimentos, o valor $d=1$ original do algoritmo mostrou-se bastante eficaz, princialmente quando um número maior de iterações foi fornecido, mesmo que tratando problemas relativamente mais complexos. Com isso, para problemas desconhecidos a serem otimizados, recomenda-se o uso de $d=1$, podendo ser alterado para valores maiores como $2,3$ ou $4$, quando não se poder ceder um grande número de iterações ao ABC, visando aumentar sua velocidade de convergência com valor de $maxCycle$ restrito. Vale apenas lembrar que, como mostrado, em geral $d<\floor{\sqrt{D}}$.

\bibliographystyle{IEEEtran}
\bibliography{references}

% that's all folks
\end{document}


