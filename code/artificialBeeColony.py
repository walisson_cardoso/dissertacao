import numpy as np


class ArtificialBeeColony:
    def __init__(self, NP, limit, evalFunc, dim, boundaries):
        # Constructor receives the maximum number of bees in the swarm, the maximum number
        # attempts (trial), the evaluation function, number of dimensions of the problem
        # and the maximum and minimum limits of the search space.
        self.NP = NP
        self.SN = NP // 2
        self.limit = limit;
        self.evalFunc = evalFunc
        # Minimum found and its location
        self.globalMin = float("inf")
        self.globalPos = np.zeros(dim)
        # Initiates the employed bees
        self.bees = [];
        for i in range(self.SN):
            self.bees.append(Bee(evalFunc, dim, boundaries))
            self.bees[i].init()

    def execute(self, nIter):
    	for i in range(nIter):
    		self.iterate()            
    
    def iterate(self):
        self.sendEmployedBees()
        self.sendOnLookerBees()
        self.sendScoutBees()
        self.memorizeBestSource()
    
    
    def sendEmployedBees(self):
        # Causes perturbations on the employed bees' positions,
        # trying to improve them.
        for i in range(self.SN):
            self.updateSolution(i)
    
    
    def sendOnLookerBees(self):
        # Onlooker bees are sent proportionally to the fitness of the
        # actual solutions (employed bees). This function implements
        # the steps: 1_ calculate probability, 2_ send onlookers
        
        # Calculate probability of solutions are preferred by the onlookers
        maxFit = self.bees[0].fitness;
        for bee in self.bees:
            if bee.fitness > maxFit:
                maxFit = bee.fitness
        
        prob = np.zeros(self.SN)
        for i in range(self.SN):
            fit = self.bees[i].fitness
            prob[i] = (0.9*(fit/maxFit))+0.1
        
        # Send onlooker bees to the solutions
        i = 0
        t = 0
        while t < self.SN:
            if np.random.rand() < prob[i]:
                t += 1
                self.updateSolution(i)
            i += 1
            i = i % self.SN
    
    
    def sendScoutBees(self):
        # Firstly, selects the bee which had more unsuccessful attempts
        # to improve a solution.
        # Then, transforms this bee on a scout. We will have at most one
        # scout per iteration.
        maxTrialIndex = 0;
        for i in range(self.SN):
            if self.bees[i].trial > self.bees[maxTrialIndex].trial:
                maxTrialIndex = i
        
        if self.bees[maxTrialIndex].trial >= self.limit:
            self.bees[maxTrialIndex].init()
    
    
    def memorizeBestSource(self):
        # Memorizes best solution found
        for bee in self.bees:
            if bee.f < self.globalMin:
                self.globalMin = bee.f

                self.globalPos = np.copy(bee.x)
    
    
    def updateSolution(self, i):
        # Does the perturbation of bee i's position
        neighbor = np.random.randint(self.SN)
        while self.SN > 1 and neighbor == i:
            neighbor = np.random.randint(self.SN)
        
        self.bees[i].perturbation(self.bees[neighbor].x)
        
    def swarm_stat(self):
        fit = np.zeros(self.SN)
        for i in range(self.SN):
            fit[i] = self.bees[i].f
        
        return (fit.mean(), fit.std())


# Bee element
class Bee:
    def __init__(self, evalFunc, dim, boundaries):
        # The constructor receives a functions of evaluation, number of dimensions
        # of the problem and the minimum and maximum limits of the search space.
        self.evalFunc = evalFunc
        self.D = dim
        self.lb = boundaries[0]
        self.ub = boundaries[1]
        # Initializes the bee's position, fitnesses' values and 
        # objective function value on the bee.
        # Trial is initialized with zero.
        self.x = np.zeros(dim)
        self.fitness = float("-inf")
        self.f = float("inf")
        self.trial = 0
    
    
    def init(self):
        # Initiates the bee's position e calculates its fitness
        # (or reinitiates its position, in case it became a scout).
        self.x = np.random.uniform(self.lb, self.ub, self.D);
        self.f = self.evalFunc(self.x)
        self.fitness = self.calcFitness(self.f)
        self.trial = 0;

    
    def perturbation(self, refX):
        # Modifies the bee's position using the reference bee received (refX)
        # It selects randomly one coordinate of the space to be modified, verifies
        # the modification and updates the position only if the fitness is improved.
        newX = np.copy(self.x)
        pc = np.random.randint(self.D)
        shift = (np.random.rand()-0.5)*2 #[-1,+1]
        
        newX[pc] = newX[pc] + (newX[pc]-refX[pc])*shift
        newX[pc] = np.clip(newX[pc], self.lb, self.ub)
        
        newF = self.evalFunc(newX)
        newFitness = self.calcFitness(newF)
        # Updates the position if the fitness is improved.
        if newFitness > self.fitness:
            self.trial = 0
            self.x = np.copy(newX)
            self.f = newF
            self.fitness = newFitness
        else:
            self.trial += 1
            
    
    def calcFitness(self, f):
        # The fitness computation is directly based on the objective function.
        # There are no negative fitnesses. This is done in order to avoid
        # problems with the proportional selection.
        fitness = 0.0
        if f >= 0:
            fitness = 1.0/(f+1)
        else:
            fitness = 1.0 + abs(f)
        
        return fitness;

