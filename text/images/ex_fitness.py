import numpy as np
import matplotlib.pyplot as plt


def fitABC(f_vec):
	fitness = []
	for f in f_vec:
		if f >= 0:
			r = 1.0/(f+1)
			fitness.append(r)
		else:
			r = 1.0 + abs(f)
			fitness.append(r)
	return fitness


f = np.linspace(-10,10,num=100)
y = fitABC(f)

plt.figure(figsize=(8,3))
plt.plot(f,y)
plt.ylabel('$\mathcal{F}(X_i)$',fontsize=14)
plt.xticks([-10, 0, 10],['$f$ $(X_i)=-10$', '$f$ $(X_i)=0$', '$f$ $(X_i)=10$'])
plt.xticks(fontsize=12)
plt.plot([0, 0], [0,11.5], color='gray', linewidth=0.5, linestyle='--')
plt.ylim(0,11.5)
plt.show()
