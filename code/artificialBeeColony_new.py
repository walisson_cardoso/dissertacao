import numpy as np


class ArtificialBeeColony:
    def __init__(self, NP, limit, evalFunc, dim, boundaries):
        self.NP = NP
        self.SN = NP // 2
        self.limit = limit;
        self.evalFunc = evalFunc
        self.globalMin = float("inf")
        self.globalPos = np.zeros(dim)
        self.bestBee = None
        self.bees = [];
        self.lb = boundaries[0]
        self.ub = boundaries[1]
        self.dim = dim
        for i in range(self.SN):
            self.bees.append(Bee(evalFunc, dim, boundaries))
            self.bees[i].init()
            
        self.nDim = 1
        self.rejections = 0
        self.prob = 0.0
        

    def execute(self, nIter):
    	for i in range(nIter):
    		self.iterate()            
    
    def iterate(self):
        self.rejections = 0;
        self.sendEmployedBees()
        self.sendOnLookerBees()
        self.sendScoutBees()
        self.memorizeBestSource()


    
    def sendEmployedBees(self):
    	if self.bestBee != None and np.random.random() < self.prob:
        	self.bestBee.gradientDescent()
        else:
			for i in range(self.SN):
				self.updateSolution(i)
    
    
    def sendOnLookerBees(self):
        maxFit = self.bees[0].fitness;
        for bee in self.bees:
            if bee.fitness > maxFit:
                maxFit = bee.fitness
        
        prob = np.zeros(self.SN)
        for i in range(self.SN):
            fit = self.bees[i].fitness
            prob[i] = (0.9*(fit/maxFit))+0.1
        
        i = 0
        t = 0
        while t < self.SN:
            if np.random.rand() < prob[i]:
                t += 1
                self.updateSolution(i)
            i += 1
            i = i % self.SN
    
    
    def sendScoutBees(self):
        maxTrialIndex = 0;
        for i in range(self.SN):
            if self.bees[i].trial > self.bees[maxTrialIndex].trial:
                maxTrialIndex = i
        
        if self.bees[maxTrialIndex].trial >= self.limit:
            self.bees[maxTrialIndex].init()
            
    
    def memorizeBestSource(self):
        for bee in self.bees:
            if bee.f < self.globalMin:
                self.globalMin = bee.f
                self.globalPos = np.copy(bee.x)
                self.bestBee = bee
                bee.isBest = True
            else:
            	bee.isBest = False
    
    
    def updateSolution(self, i):
        neighbor = np.random.randint(self.SN)
        while self.SN > 1 and neighbor == i:
            neighbor = np.random.randint(self.SN)
        
        didImprove = self.bees[i].perturbation(self.bees[neighbor].x, self.nDim)
        if not didImprove:
        	self.rejections += 1
   
        
    def swarm_stat(self):
        fit = np.zeros(self.SN)
        for i in range(self.SN):
            fit[i] = self.bees[i].f
        
        return (fit.mean(), fit.std())
        
    
    def popDiversity(self):
		# A diversity measure for the population. More details in:
		# A Diversity-Guided Particle Swarm Optimizer - the ARPSO
		# Ridget, J. and Vestertrom, J. S. (2002)

		S = self.SN                             # Constant elements in the swarm
		L = abs(self.ub-self.lb) * np.sqrt(2)   # Biggest diagonal
		D = self.dim                            # Dimensionality of the problem

		pm = [0.0]*(D)                                  # Mean point

		for j in range(D):
			for i in range(S):
				pm[j] += self.bees[i].x[j]
			pm[j] = pm[j] / S
	
		diversity = 0.0
		for i in range(S):
			var = 0.0
			for j in range(D):
				var += (self.bees[i].x[j] - pm[j]) ** 2
			diversity += np.sqrt(var)
	
		return diversity/(S*L)




class Bee:
	def __init__(self, evalFunc, dim, boundaries):
		self.evalFunc = evalFunc
		self.D = dim
		self.lb = boundaries[0]
		self.ub = boundaries[1]
		self.x = np.zeros(dim)
		self.fitness = float("-inf")
		self.f = float("inf")
		self.trial = 0
		self.isBest = False
		self.alpha = 1


	def init(self):
		self.x = np.random.uniform(self.lb, self.ub, self.D);
		self.f = self.evalFunc(self.x)
		self.fitness = self.calcFitness(self.f)
		self.trial = 0;


	def perturbation(self, refX, nDim):
		newX = np.copy(self.x)
		for i in range(nDim):
			shift = (np.random.rand()-0.5)*2 #[-1,+1]
			pc = np.random.randint(self.D)
			newX[pc] = newX[pc] + (newX[pc]-refX[pc])*shift
			newX[pc] = np.clip(newX[pc], self.lb, self.ub)
		
		newF = self.evalFunc(newX)
		newFitness = self.calcFitness(newF)
		if newFitness > self.fitness:
			self.trial = 0
			self.x = np.copy(newX)
			self.f = newF
			self.fitness = newFitness            
			return True
		else:
			self.trial += 1
			return False


	def gradientDescent(self):
		newX = np.copy(self.x)
		self.isBest = False
		deriv = self.gradient(self.x, self.f)
		newX = np.copy(self.x - self.alpha*deriv)
		
		newF = self.evalFunc(newX)
		newFitness = self.calcFitness(newF)
		if newFitness > self.fitness:
			self.x = np.copy(newX)
			self.f = newF
			self.fitness = newFitness
		else:
			self.alpha /= 2.0
		


	def calcFitness(self, f):
		fitness = 0.0
		if f >= 0:
			fitness = 1.0/(f+1)
		else:
			fitness = 1.0 + abs(f)

		return fitness;


	def gradient(self, x, f_x):
		deriv = np.zeros(self.D)
		step = 1e-8
		
		for c in range(self.D):
			x_d = np.copy(x)
			x_d[c] = x_d[c]+step
			f_d = self.evalFunc(x_d)
			deriv[c] = (f_d-f_x)/step

		return deriv

