# -*- coding: utf-8 -*-

# This script loads a bunch of files resulting from several optimizations and plots them
# This function is in accordance with the verify_dimensionality script

# Imports
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from problems import optFunction

# How many functions, dimensions of the problem, population size NP, maxCycle and repetition for configutation
nFuncs = 12
repetitions = 10


for nIter in [500,2000]:
	for popSize in [20,50,80]:
		for nDim in [10,20,30]:
			best_d = np.zeros(nDim)
			
			for p in range(nFuncs):
				funcCal, limits, funcName = optFunction(p)
				folderName = '/home/gomes/Documents/Mestrado/Dissertação - Dados/' + str(popSize) + 'ind/'
				fileName = folderName + funcName + '_' + str(nDim) + 'd_' + str(popSize) + 'ind.txt'
				data = np.loadtxt(fileName,delimiter=',')
	
				meanVal = np.zeros(nDim)
				stdVal = np.zeros(nDim)
				for i in range(nDim):
					data_d = data[i*repetitions:(i+1)*repetitions]
		
					values = np.zeros(repetitions)
					for iter,d in enumerate(data_d):
						values[iter] = d[nIter-1]
		
					mean = values.mean()
					std = values.std()
					meanVal[i] = mean
					stdVal[i] = std
	
				r2 = [meanVal.min(), meanVal.max()]
				for i,val in enumerate(meanVal):
					if abs(val - r2[0]) <= 1e-30:
						best_d[i] += 1

			print(sum(best_d[0:nDim/2]), sum(best_d[nDim/2:-1]))
	print("")
