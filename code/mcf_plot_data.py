# -*- coding: utf-8 -*-

# This script loads a bunch of files resulting from several optimizations and plots them
# This function is in accordance with the verify_dimensionality script

# Imports
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from problems import optFunction
from scipy.optimize import curve_fit

def func(x, a):
    return a*x+1

def optimized(x, NP):
    return np.round(NP*3.3195358953589546e-07*x+1)

# How many functions, dimensions of the problem, population size NP, maxCycle and repetition for configutation
nFuncs = 12
repetitions = 10

plt.figure(figsize=(6,4))

foldername = '/home/gomes/Documents/gitRepo/Dissertacao/code/data/'
filepath = foldername + 'best_d_by_cicle.txt'
data = np.loadtxt(filepath, delimiter=',')

mcf = data[0]

for i, popSize in enumerate([20,50,80]):
    l = '$NP$=' + str(popSize)
    plt.plot(mcf, data[i+1]+1, label=l)

x = np.append(mcf, np.append(mcf,mcf))
y = np.append(data[1]+1, np.append(data[2]+1, data[3]+1))

popt, pcov = curve_fit(func, x, y, maxfev=8000)
plt.plot(mcf, func(mcf, *popt), 'r--', linewidth=2, label='Curva ajustada')
print(popt)

plt.xlabel('MCF')
plt.ylabel('$d$')
plt.legend()
plt.show()

# Valores do ajuste de reta:
# [4.55789652e-05]
# [4.08630273e-05]
# [2.65560519e-05]
# Coeficiente final:
# min(np.round(3.77e-05*mcf + 1), $d$)

