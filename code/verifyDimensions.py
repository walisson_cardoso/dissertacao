import numpy as np
import matplotlib.pyplot as plt
import numpy as np
from problems import *
from artificialBeeColony_new import ArtificialBeeColony as ABCD

np.random.seed(1) # For reproductibility

nRuns = 10
iterations = 5000
dimensionality = 40

for popSize in [20, 50, 80]:
    limit = (popSize//2)*dimensionality

    for func in range(12):

	    evalFunc, boundaries, name = optFunction(func)
	    file = open('data/'+name+'_'+str(dimensionality)+'d'+'_'+str(popSize)+'ind'+'.txt', 'w')

	    for d in range(1,dimensionality+1):
		    for run in range(nRuns):
			    min_found = []
			    abcd = ABCD(popSize, limit, evalFunc, dimensionality, boundaries)
			    abcd.nDim = d
		
			    for i in range(iterations):
				    abcd.iterate()
				    min_found.append(abcd.globalMin)
		
			    file.write(str(min_found)[1:-1])
			    file.write("\r\n")
		    file.write("\r\n")

	    file.close()
