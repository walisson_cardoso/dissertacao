import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker


nDim = 20
nIter = 1000
fileName = str(nDim) + 'dim_30pop_' + str(nIter) + 'iter'
data = np.loadtxt(fileName,delimiter=',')

fig, axes = plt.subplots(8,1,figsize=(5,8))
plt.subplots_adjust(hspace=0.45)

for p in range(8):
	meanVal = np.zeros(nDim)
	stdVal = np.zeros(nDim)
	for i in range(nDim):
		values = data[p*nDim+i]
		mean = values.mean()
		std = values.std()
		meanVal[i] = mean
		stdVal[i] = std

	r1 = [meanVal.min(), meanVal.max()]
	if r1[0] <= 0:
		meanVal = meanVal+abs(r1[0])+1e-30
	r2 = [meanVal.min(), meanVal.max()]
	ax = axes[p]
	ax.grid(which='major', color='gray', linestyle='dotted')
	ax.set_yticks(r2)
	
	smin = np.format_float_scientific(r1[0],precision=2)
	smax = np.format_float_scientific(r1[1],precision=2)
	ax.set_yticklabels([smin,smax])
	
	ax.set_xticks(np.arange(nDim/10,nDim+1,nDim/10))
	if p < 7:
		ax.tick_params(axis='x', colors='white')
	
	label = '$f_{' + str(p+1) + '}(X^{*})$'
	ax.set_ylabel(label)
	ax.yaxis.tick_right()
	
	x = range(1,nDim+1)
	ax.plot(x,meanVal,'black')
	#ax.fill_between(x, meanVal+stdVal,meanVal-stdVal,facecolor='blue',alpha=0.3)
	for i,val in enumerate(meanVal):
		if abs(val - r2[0]) <= 1e-30:
			ax.plot(i+1, val, 'ro')

plt.tight_layout()
plt.show()
