# -*- coding: utf-8 -*-

# This script loads a bunch of files resulting from several optimizations and plots them
# This function is in accordance with the verify_dimensionality script

# Imports
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from problems import optFunction

# How many functions, dimensions of the problem, population size NP, maxCycle and repetition for configutation
nFuncs = 12
repetitions = 10

for nDim in [10,20,30]:

	avgMean = np.zeros(nDim)
	avgStd = np.zeros(nDim)
	for nIter in [500]:
		for popSize in [20, 50, 80]:
			for p in range(nFuncs):
				funcCal, limits, funcName = optFunction(p)
				folderName = '/home/gomes/Documents/Mestrado/Dissertação - Dados/' + str(popSize) + 'ind/'
				fileName = folderName + funcName + '_' + str(nDim) + 'd_' + str(popSize) + 'ind.txt'
				data = np.loadtxt(fileName, delimiter=',')
				
				meanVal = np.zeros(nDim)
				stdVal  = np.zeros(nDim)
				for i in range(nDim):
					data_d = data[i*repetitions:(i+1)*repetitions]
			
					values = np.zeros(repetitions)
					for iter,d in enumerate(data_d):
						values[iter] = d[nIter-1]
					
					meanVal[i] = values.mean()
					stdVal[i] = values.std()
				
				if meanVal.max() - meanVal.min() != 0.0:
					avgMean += (meanVal - meanVal.min())/(meanVal.max() - meanVal.min())
					avgStd += (stdVal - stdVal.min())/(stdVal.max() - stdVal.min())
	
		avgMean /= nDim*3
		avgStd  /= nDim*3
	
	l = ['$d$ = '+str(nDim)]
	plt.plot(range(1,nDim+1), avgMean, label=l[0])
	plt.fill_between(range(1,nDim+1), avgMean+avgStd, avgMean-avgStd, facecolor='blue', alpha=0.1)
	plt.yticks([])
	plt.xticks(np.arange(nDim/10,nDim+1,nDim/10))
	plt.legend()
	
	name = 'mean_'+ str(nDim) + '_d.pdf'
	plt.savefig(name)
	#plt.gcf().clear()
