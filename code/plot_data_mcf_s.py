# -*- coding: utf-8 -*-

# This script loads a bunch of files resulting from several optimizations and plots them
# This function is in accordance with the verify_dimensionality script

# Imports
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from problems import optFunction

# How many functions, dimensions of the problem, population size NP, maxCycle and repetition for configutation
nFuncs = 12
repetitions = 10

for mcf in [10000, 75000, 150000]:
    for nDim in [10, 20, 30]:
        folderName = '/home/gomes/Documents/Mestrado/Back up Mestrado/mcf/'
        filepath = folderName + str(nDim) + 'd_' + str(mcf) + 'mcf.txt'
        data = np.loadtxt(filepath, delimiter=',')
        
        popSize = []
        for p in data[0]:
            popSize.append(int(p))
        nPop = len(popSize)
        
        avgMean = np.zeros(nPop)
    	avgStd = np.zeros(nPop)
        
        for f in range(nFuncs):
            funcCal, limits, funcName = optFunction(f)
            
            data_f = data[f*repetitions+1:(f+1)*repetitions+1]
            
            meanVal = np.mean(data_f, axis=0)
            stdVal = np.std(data_f, axis=0)
            
            if meanVal.max() - meanVal.min() != 0.0:
                avgMean += (meanVal - meanVal.min())/(meanVal.max() - meanVal.min())
                avgStd += (stdVal - stdVal.min())/(stdVal.max() - stdVal.min())
            
        avgMean /= 3*nFuncs
        avgStd  /= 3*nFuncs
    
    l = ['$d$ = '+str(nDim)]
    plt.plot(popSize, avgMean, label=l[0])
    plt.fill_between(popSize, avgMean+avgStd, avgMean-avgStd, facecolor='blue', alpha=0.1)
    plt.xlabel('MCF', fontsize=13)
    plt.ylabel('M' + r'$\acute{e}$' + 'dia', fontsize=13)
    
    plt.xticks(popSize)
    plt.locator_params(axis='x', nbins=5)
    #plt.legend()
    
    name = 'mean_' + str(mcf) + 'mcf.pdf'
    plt.savefig(name)
    plt.gcf().clear()
