# -*- coding: utf-8 -*-

# This script loads a bunch of files resulting from several optimizations and plots them
# This function is in accordance with the verify_dimensionality script

# Imports
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from problems import optFunction

# How many functions, dimensions of the problem, population size NP, maxCycle and repetition for configutation
nFuncs = 12
repetitions = 10

mcf = range(1000,250000,1000)

file = open('best_d_by_cicle.txt', 'w')
file.write(str(mcf)[1:-1])
file.write("\r\n")

for popSize in [20,50,80]:
    final_d = []
    for m in mcf:
        nIter = int(m/popSize)
        print((popSize, m, nIter))

        best_d = []
        for nDim in [10,20]:
            avgMean = np.zeros(nDim)
            avgStd = np.zeros(nDim)
            for p in range(nFuncs):
                funcCal, limits, funcName = optFunction(p)
                folderName = '/home/gomes/Documents/Mestrado/Dissertação - Dados/' + str(popSize) + 'ind/'
                fileName = folderName + funcName + '_' + str(nDim) + 'd_' + str(popSize) + 'ind.txt'
                data = np.loadtxt(fileName, delimiter=',')
			
                meanVal = np.zeros(nDim)
                stdVal  = np.zeros(nDim)
                for i in range(nDim):
                    data_d = data[i*repetitions:(i+1)*repetitions]

                    values = np.zeros(repetitions)
                    for iter,d in enumerate(data_d):
                        values[iter] = d[nIter-1]
				
                    meanVal[i] = values.mean()
                    stdVal[i] = values.std()
			
                if meanVal.max() - meanVal.min() != 0.0:
                    avgMean += (meanVal - meanVal.min())/(meanVal.max() - meanVal.min())
                    avgStd += (stdVal - stdVal.min())/(stdVal.max() - stdVal.min())
	
            best_d.append(np.argmin(avgMean))
	
        final_d.append(sum(best_d)/len(best_d))
	
    l = '$NP$=' + str(popSize)
    plt.plot(mcf, final_d, label=l)
    file.write(str(final_d)[1:-1])
    file.write("\r\n")

file.close()
plt.legend()
plt.show()
