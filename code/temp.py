import matplotlib.pyplot as plt
import numpy as np

def testX(x):
	res  = abs(x*20-4.55789652e-05)
	res += abs(x*50-4.08630273e-05)
	res += abs(x*80-2.65560519e-05)

	return res

start = -3.31e-7
end = 3.32e-7

min_error = 1e10
pos_min = 10
for x in np.linspace(start, end,100000):
	if testX(x) < min_error:
		pos_min = x
		min_error = testX(x)

x = np.linspace(start, end, 1000)
plt.plot(x,testX(x))
plt.show()
print(pos_min)
