# -*- coding: utf-8 -*-

# This script loads a bunch of files resulting from several optimizations and plots them
# This function is in accordance with the verify_dimensionality script

# Imports
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from problems import optFunction

# How many functions, dimensions of the problem, population size NP, maxCycle and repetition for configutation
nFuncs = 12
repetitions = 10

plt.figure(figsize=(4,4))


shit = []

for nDim in [10,20,30]:
	avgMean = np.zeros(nDim)
	avgStd = np.zeros(nDim)
	for popSize in [80]:
		for nIter in [500,2000,5000]:
			for p in range(nFuncs):
				funcCal, limits, funcName = optFunction(p)
				folderName = '/home/gomes/Documents/Mestrado/Back up Mestrado/' + str(popSize) + 'ind/'
				fileName = folderName + funcName + '_' + str(nDim) + 'd_' + str(popSize) + 'ind.txt'
				data = np.loadtxt(fileName, delimiter=',')
				
				meanVal = np.zeros(nDim)
				stdVal  = np.zeros(nDim)
				for i in range(nDim):
					data_d = data[i*repetitions:(i+1)*repetitions]
			
					values = np.zeros(repetitions)
					for iter,d in enumerate(data_d):
						values[iter] = d[nIter-1]
					
					meanVal[i] = values.mean()
					stdVal[i] = values.std()
				
				if meanVal.max() - meanVal.min() != 0.0:
					avgMean += (meanVal - meanVal.min())/(meanVal.max() - meanVal.min())
					avgStd += (stdVal - stdVal.min())/(stdVal.max() - stdVal.min())
	
		avgMean /= nDim*3
		avgStd  /= nDim*3
		
		if True:
		    plt.gcf().clear()
		    l = ['$D$ = '+str(nDim)]
		    plt.plot(range(1,nDim+1), avgMean, label=l[0])
		    plt.fill_between(range(1,nDim+1), avgMean+avgStd, avgMean-avgStd, facecolor='blue', alpha=0.1)
		    plt.xticks(np.arange(nDim/10,nDim+1,nDim/10))
		    plt.xlabel('$d$')
		    plt.ylabel('M' + r'$\acute{e}$' + 'dia')
		    plt.tight_layout()
		
		    name = 'mean_'+ str(popSize) + 'pop_' + str(nDim) + 'd.pdf'
		    plt.savefig(name)
		    plt.gcf().clear()
        else:
		    l = ['$D$ = '+str(nDim)]
		    plt.plot(range(1,nDim+1), avgMean, label=l[0])
		    plt.fill_between(range(1,nDim+1), avgMean+avgStd, avgMean-avgStd, facecolor='blue', alpha=0.1)
		    shit.append(avgMean.argmin())
		    print(shit)
		
		    if nDim == 30:
		        best_d = np.round((np.array(shit)).mean())+1
		        bottom, top = plt.ylim()
		        plt.plot([best_d, best_d],[bottom, top],linestyle='--', color=(0.1, 0.2, 0.5, 0.3))
		        
		        plt.xticks(np.arange(nDim/10,nDim+1,nDim/10))
		        plt.xlabel('$d$')
		        plt.ylabel('M' + r'$\acute{e}$' + 'dia')
		        plt.tight_layout()
		        plt.legend()
		        name = 'mean_'+ str(popSize) + 'pop.pdf'
		        plt.savefig(name)
