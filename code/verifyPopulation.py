import numpy as np
import matplotlib.pyplot as plt
import numpy as np
from problems import *
from artificialBeeColony_new import ArtificialBeeColony as ABCD

np.random.seed(1) # For reproductibility

nRuns = 10

for dimensionality in [10, 20, 30]:
    for mcf in [50000]:
        maxpop = np.round(np.sqrt(mcf))
        poprange = np.round(np.linspace(4, maxpop, 20))
        
        file = open('data/'+str(dimensionality)+'d'+'_'+str(mcf)+'mcf'+'.txt', 'w')
        file.write(str(poprange)[1:-1])
        file.write("\r\n")

        for func in range(12):
            evalFunc, boundaries, name = optFunction(func)

            for run in range(nRuns):
                min_found = []
                for popSize in poprange:
                    limit = (popSize//2)*dimensionality
                    abcd = ABCD(int(popSize), limit, evalFunc, dimensionality, boundaries)
                    abcd.nDim = int(min(np.round(3.77e-05*mcf + 1), dimensionality))

                    iterations = int(mcf/popSize)
                    for i in range(iterations):
                        abcd.iterate()
                    min_found.append(abcd.globalMin)

                file.write(str(min_found)[1:-1])
                file.write("\r\n")
            file.write("\r\n")

        file.close()
