\changetocdepth {4}
\select@language {brazil}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\IeC {\c c}\IeC {\~a}o}}{15}{chapter.1}
\contentsline {section}{\numberline {1.1}Trabalhos Relacionados}{17}{section.1.1}
\contentsline {section}{\numberline {1.2}Motiva\IeC {\c c}\IeC {\~a}o}{19}{section.1.2}
\contentsline {section}{\numberline {1.3}Justificativa}{20}{section.1.3}
\contentsline {section}{\numberline {1.4}Objetivo Geral}{21}{section.1.4}
\contentsline {section}{\numberline {1.5}Objetivos Espec\IeC {\'\i }ficos}{21}{section.1.5}
\contentsline {section}{\numberline {1.6}Estrutura da Disserta\IeC {\c c}\IeC {\~a}o}{22}{section.1.6}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Col\IeC {\^o}nia Artificial de Abelhas}}{23}{chapter.2}
\contentsline {section}{\numberline {2.1}Inicializa\IeC {\c c}\IeC {\~a}o}{25}{section.2.1}
\contentsline {section}{\numberline {2.2}Fase das Empregadas}{26}{section.2.2}
\contentsline {section}{\numberline {2.3}Fase das Observadoras}{28}{section.2.3}
\contentsline {section}{\numberline {2.4}Fase das Escoteiras}{30}{section.2.4}
\contentsline {section}{\numberline {2.5}Memorizar Melhor Solu\IeC {\c c}\IeC {\~a}o}{32}{section.2.5}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Col\IeC {\^o}nia Artificial de Abelhas com Ajuste de Par\IeC {\^a}metros}}{33}{chapter.3}
\contentsline {section}{\numberline {3.1}N\IeC {\'u}mero de Dimens\IeC {\~o}es na Equa\IeC {\c c}\IeC {\~a}o de Busca}{33}{section.3.1}
\contentsline {section}{\numberline {3.2}Tamanho da popula\IeC {\c c}\IeC {\~a}o}{35}{section.3.2}
\contentsline {section}{\numberline {3.3}Algoritmo Final}{37}{section.3.3}
\contentsline {section}{\numberline {3.4}Fun\IeC {\c c}\IeC {\~o}es de Avalia\IeC {\c c}\IeC {\~a}o}{38}{section.3.4}
\contentsline {section}{\numberline {3.5}Ambiente de Testes}{42}{section.3.5}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Resultados e An\IeC {\'a}lise}}{43}{chapter.4}
\contentsline {section}{\numberline {4.1}N\IeC {\'u}mero de Dimens\IeC {\~o}es na Equa\IeC {\c c}\IeC {\~a}o de Busca}{43}{section.4.1}
\contentsline {section}{\numberline {4.2}Tamanho da popula\IeC {\c c}\IeC {\~a}o}{50}{section.4.2}
\contentsline {section}{\numberline {4.3}C\IeC {\'a}lculo dos par\IeC {\^a}metros}{53}{section.4.3}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{CONCLUS\IeC {\~A}O}{54}{chapter*.51}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^encias}}{55}{section*.53}
\renewcommand *{\cftappendixname }{AP\^ENDICE \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{\uppercase {Ap\^endices}}{59}{section*.54}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}\MakeTextUppercase {M\IeC {\'e}dia dos resultados por fun\IeC {\c c}\IeC {\~a}o variando $d$.}}{60}{appendix.A}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {B}\MakeTextUppercase {M\IeC {\'e}dia dos resultados por fun\IeC {\c c}\IeC {\~a}o variando $NP$.}}{70}{appendix.B}
