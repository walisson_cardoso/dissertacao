\documentclass[aspectratio=169]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{utopia} %font utopia imported
\usepackage{array}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

\usepackage{algorithm2e}
\usepackage{algorithmic}
\usepackage{scalerel}
\usepackage{adjustbox}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage[outdir=./images/]{epstopdf}

\usepackage{amsmath}
\usepackage{mathtools}

\usepackage[notocbib]{apacite}

% Pseudo-algs
\usepackage{algorithm2e}
\SetKwInput{KwIn}{Entrada}
\SetKwInput{KwOut}{Saída}
\SetKwInput{KwData}{Dados}
\SetKwInput{KwResult}{Resultado}
\SetKwRepeat{Repeat}{repetir}{enquanto}
\SetKwFor{ForEach}{para todo}{faça}{fim}
\SetKwIF{If}{ElseIf}{Else}{se}{então}{senão, se}{senão}{fim}
\SetKwBlock{Begin}{início}{fim}
% 

\usetheme{Copenhagen}
\usecolortheme{seahorse}
\setbeamertemplate{headline}{}


%Margin modification
\newcommand\Wider[2][3em]{%
	\makebox[\linewidth][c]{%
		\begin{minipage}{\dimexpr\textwidth+#1\relax}
			\raggedright#2
		\end{minipage}%
	}%
}

\renewcommand{\figurename}{Fig.}
\renewcommand{\tablename}{Tab.}

\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

%------------------------------------------------------------
%This block of code defines the information to appear in the
%Title page

\logo{\includegraphics[width=1cm]{images/ufpa.png}}

\title[Colônia Artificial de Abelhas com Parametrização Adaptativa] %optional
{Colônia Artificial de Abelhas com Parametrização Adaptativa}

\subtitle{}

\author[Walisson Cardoso Gomes] % (optional)
{Walisson Cardoso Gomes \\
	\bigskip
	Orientador: Claudomiro de Souza de Sales Junior \\
	Coorientador: Reginaldo Cordeiro dos Santos Filho}

\institute[UFPA] % (optional)
{
	Instituto de Ciências Exatas e Naturais \\
	Universidade Federal do Pará
}

\date{15 de Março de 2019}

%End of title page configuration block
%------------------------------------------------------------



%------------------------------------------------------------
%The next block of commands puts the table of contents at the 
%beginning of each section and highlights the current section:

\AtBeginSection[]
{
	\begin{frame}
		\frametitle{Agenda}
		\tableofcontents[currentsection]
	\end{frame}
}
%------------------------------------------------------------

\begin{document}
	
	%The next statement creates the title page.
	\frame{\titlepage}
	
	
	%---------------------------------------------------------
	%This block of code is for the table of contents after
	%the title page
	\begin{frame}
		\frametitle{Agenda}
		\tableofcontents
	\end{frame}
	
	%---------------------------------------------------------
	
	\section{Introdução}
	
	\begin{frame}
		\frametitle{Introdução}
		
		Existe uma grande variedade de problemas de otimização no mundo real.
		
		\bigskip
		
		Muitos destes problemas são difíceis de resolver e não podem ser computados em tempo razoável.
		
		\bigskip
		
		Daí surge a necessidade de métodos aproximados e heurísticas.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Introdução}
		
		As meta-heurísticas são técnicas que guiam uma heurística subordinada pela combinação de processos inteligentes de exploração do espaço de soluções \shortcite{Osman1995}.
		
		\bigskip
		
		Dois critérios contraditórios devem ser levados em consideração: exploração do espaço de busca e investigação das melhores soluções encontradas (\textit{exploration} e \textit{explotation}).
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Introdução}
		
		Algumas classes de meta-heurísticas vêm obtendo certa notoriedade:
		\begin{itemize}
			\item Algoritmos evolucionários.
			\item Algoritmos de inteligência de enxame.
		\end{itemize}
		
		\bigskip
		
		\citeA{Karaboga2005} desenvolveu uma técnica de otimização chamada Colônia Artificial de Abelhas (\textit{Artificial Bee Colony} -- ABC) inspirada no comportamento natural de abelhas à procura de fontes de alimento rentáveis.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Introdução}
		
		Alguns fatores que tornaram o ABC uma técnica popular são:
		\begin{enumerate}
			\item Número reduzido de parâmetros de configuração.
			\item Facilidade de implementação.
			\item Eficiência.
		\end{enumerate}
		
		\bigskip
		
		A técnica conta com apenas três parâmetros livres de configuração:
		\begin{enumerate}
			\item Tamanho da população.
			\item Número de iterações.
			\item \textit{limit}.
		\end{enumerate}
		
	\end{frame}
	
	%---------------------------------------------------------
	
	\begin{frame}
		\frametitle{Trabalhos Relacionados}
		
		Na literatura, é muito comum a criação de novas versões do ABC e/ou ajuste de parâmetros.
		
		\bigskip
		
		Entre os possíveis ajustes de parâmetros, considera-se:
		\begin{itemize}
			\item Verificação do número $d$ de dimensões trabalhadas simultaneamente na equação de busca do ABC.
			\item Estudo do tamanho de população ideal.
		\end{itemize}
		
	\end{frame}
	
	
	\begin{frame}
		\frametitle{Trabalhos Relacionados}
		
		\shortciteA{Akay2012} propõem a criação de um parâmetro $MR$ para controlar $d$.
		
		\bigskip
		
		No ABC original, apenas uma coordenada de uma solução é alterada por execução da equação.
		
		\bigskip
		
		Se $MR=0.1$, aproximadamente 10\% das coordenadas serão modificadas.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Trabalhos Relacionados}
		
		\shortciteA{Xiang2017} propõe a retirada do parâmetro $MR$, fazendo $d=\sqrt{D}$, onde $D$ é a dimensionalidade do problema.
		
		\bigskip
		
		Ambos os trabalhos, além da estipulação de $d$, realizam, cada um, uma modificação à técnica.
		
		\bigskip
		
		Esta dissertação põe à prova a afirmação feita em \shortcite{Xiang2017}, verificando um valor mais apropriado para $d$.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Trabalhos Relacionados}
		
		Sobre o tamanho da população ideal, em \shortcite{Karaboga2008} os autores realizam testes comparativos e determinam que a população deve ter entre 50 e 100 indivíduos.
		
		\bigskip
		
		Já em \shortcite{Diwold2011}, os autores criticam o trabalho anterior ao afirmar que o tamanho da população deve levar em consideração o número máximo de consultas à função objetivo permitido.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Motivação}
		
		As meta-heurísticas são geralmente empregadas como caixas-pretas, abrangendo uma vasta gama de problemas.
		
		\bigskip
		
		Devido a praticidade e aplicabilidade, o número de publicações na área têm crescido exponencialmente.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Motivação}
		
		\shortciteA{Sorensen2015} argumenta que a área está ficando saturada de ``novos'' algoritmos. Nas suas palavras, maior avanço seria obtido se os estudos se detivessem mais na teoria e melhoramento de algoritmos já existentes.
		
		\bigskip
		
		Outra questão interessante pode ser considerada levando em conta o teorema \textit{No Free Lunch} para busca e otimização, que descreve a impossibilidade de desenvolver um mecanismo de busca com efetividade superior em todos as classes de problemas.
		
		\begin{figure}[h]
			\centering
			\includegraphics[height=0.45\textheight]{images/no_free_lunch.pdf}
		\end{figure}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Justificativa}
		
		Os trabalhos citados apresentam conclusões insuficientes.
		
		\bigskip
		
		Os estudos do valor de $d$ feitos por \shortcite{Akay2009} e \shortcite{Xiang2017} são pouco úteis no que concerne à teoria da técnica.
		
		\bigskip
		
		Ao mesmo tempo, a relação entre o tamanho da população e o número de consultas à função objetivo no ABC foi lembrada, mas uma solução não foi proposta.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Justificativa}
		
		Devido à falta de desenvolvimento teórico sobre o ABC, é difícil determinar bons parâmetros para a técnica.
		
		\bigskip
		
		Este estudo é uma continuação de \shortcite{Gomes2018}.
		
		\bigskip
		
		Aqui verifica-se, sem adição de outros fatores, como os valores de $d$ respondem a diferentes funções-objetivo.
		
		\bigskip
		
		Uma vez definido este ponto, verifica-se o melhor tamanho de população dado um limite de computação permitido.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Objetivo Geral}
		
		Definição de um algoritmo Colônia Artificial de Abelhas que receba um conjunto reduzido de parâmetros de configuração e possua um desempenho médio satisfatório dado um conjunto de 12 funções objetivo selecionadas de antemão para cobrir um grupo de características desafiadoras que se consideram importantes na otimização numérica.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Objetivos Específicos}
		
		Figuram como objetivos específicos do trabalho:
		
		\begin{itemize}
			\item Execução do ABC sobre um conjunto de cenários diferentes com registro do mínimo médio para diferentes $d$.
			\item Verificar se existe um valor ou regra para definição de $d$.
			\item Executar o ABC com diferentes relações de tamanho de população e número de iterações.
			\item Verificar se existe uma regra para definição do tamanho de população.
			\item Associando os resutados encontrados com outras características da literatura, definir uma recomendação do ABC.
		\end{itemize}
		
	\end{frame}
	
	\section{Colônia Artificial de Abelhas}
	
	\begin{frame}
		\frametitle{Colônia Artificial de Abelhas}
		
		Descrito em 2005, o algoritmo referencia um modelo minimalista do comportamento de abelhas:
		
		\begin{itemize}
			\item Fontes de alimento.
			\item Forrageiras empregadas.
			\item Forrageiras desempregadas.
		\end{itemize}
		
		\bigskip
		
		O valor da fonte de alimento depende de uma função $f$, definindo seu \textit{fitness}.
		
		\bigskip
		
		O algoritmo armazena $SN$ fontes de alimento, isto é, $SN$ soluções candidatas. Cada uma possui um \textit{trial}.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Colônia Artificial de Abelhas}
		
		Cada \underline{forrageira empregada} está associada à uma fonte de alimento.
		
		\bigskip
		
		Elas compartilham informações sobre as fontes com as outras abelhas da colmeia, realizando o recrutamento.
		
		\bigskip
		
		A informação é passada através da \textit{waggle dance}.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Colônia Artificial de Abelhas}
		
		\begin{figure}[h]
			\centering
			\includegraphics[height=0.7\textheight]{images/waggle_dance.pdf}
		\end{figure}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Colônia Artificial de Abelhas}
		
		Existem dois tipos de \underline{forrageiras desempregadas}:
		
		\begin{itemize}
			\item Observadoras.
			\item Escoteiras.
		\end{itemize}
		
		As observadoras escolhem uma fonte de alimento observando várias \textit{waggle dances}. As fontes são escolhidas proporcionalmente ao seu \textit{fitness}.
		
		\bigskip
		
		As escoteiras buscam por novas fontes de alimento para explorar.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Colônia Artificial de Abelhas}
		
		\begin{figure}[h]
			\includegraphics[height=0.8\textheight]{images/abc_fases.pdf}
		\end{figure}
		
	\end{frame}
	
	\section{Colônia Artificial de Abelhas com Ajuste de Parâmetros}
	
	\begin{frame}
		\frametitle{Número de dimensões na equação de busca}
		
		Considerando a perturbação, a $i$-ésima fonte de alimento pode ser modificada uniformemente em ambas direções positiva e negativa.
		
		\begin{equation*}
		\label{eq:pos_update}
		X_{i,j}' = X_{i,j} + \phi~(X_{i,j}-X_{k,j})
		\end{equation*}
		
		\begin{figure}[h]
			\centering
			\includegraphics[height=0.4\textheight]{images/mov_d1_1.pdf}
		\end{figure}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Número de dimensões na equação de busca}
		
		\begin{equation*}
		\label{eq:pos_updat_multi}
		~~~X_{i,j}' = X_{i,j} + \phi_j~(X_{i,j}-X_{k,j})~~~\forall j \in J
		\end{equation*}
		
		\begin{figure}[h]
			\centering
			\subfloat[$d=1$]{\label{fig:pos_mov_dif_a} \includegraphics[width=0.3\textwidth]{images/mov_d1_1.pdf}}
			\hspace{0.002\textwidth}
			\subfloat[$d=2$ e $\phi_{1} \ne \phi_{2}$]{\label{fig:pos_mov_dif_b} \includegraphics[width=0.3\textwidth]{images/mov_d2_1.pdf}}
			\hspace{0.002\textwidth}
			\subfloat[$d=2$ e $\phi_{1} = \phi_{2}$]{\label{fig:pos_mov_dif_c} \includegraphics[width=0.3\textwidth]{images/mov_d2_2.pdf}}
		\end{figure}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Tamanho da população}
		
		Em condições normais, para executar o ABC, o operador precisa:
		
		\begin{itemize}
			\item Determinar o parâmetro \textit{limit}.
			\item Escolher um tamanho de população $NP$.
			\item Estipular um número de iterações $maxCycle$ com base no tempo disponível.
		\end{itemize}
		
		\bigskip
		
		No entanto, a complexidade da função objetivo tem incluência direta sobre a complexidade geral do ABC.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Tamanho da população}
		
		O número $T$ de chamadas à função objetivo $f$ é tal que
		
		\begin{equation*}
		maxCycle \times NP \le T \le maxCycle \times (NP+1)
		\end{equation*}
		
		Existem diversas combinações de $maxCycle$ e $NP$ que mantém esta relação.
		
		\bigskip
		
		É mais cômodo ao operador do algoritmo apenas estipular $T$ do que $NP$ e $maxCycle$.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Tamanho da população}
		
		\begin{figure}[h]
			\centering
			\includegraphics[height=0.7\textheight]{images/relation_pop_cycle.pdf}
		\end{figure}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Funções de avaliação}
		
		Para determinar $d$ e $NP$, foram selecionadas 12 funções objetivo frequentemente utilizadas na literatura.
		
		\begin{columns}
			\begin{column}{0.5\textwidth}
				\begin{itemize}
					\item Ackley
					\item Alpine N. 1
					\item Griewank
					\item Levy
					\item Michalewicz
					\item Rastrigin
				\end{itemize}
			\end{column}
			\begin{column}{0.5\textwidth}
				\begin{itemize}
					\item Rosenbrock
					\item Schwefel
					\item Shubert 3
					\item Styblinski-Tank
					\item Sum of Squares
					\item Zakharov
				\end{itemize}
			\end{column}
		\end{columns}
		
		
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Ambiente de testes}
		
		Experimentos realizados em um computador com:
		\begin{itemize}
			\item Intel$^{\text{\textregistered}}$ Core i7-4700MQ, velocidade 2.5 GHz.
			\item 8 GB de memória RAM.
			\item Ubuntu 16.04.
		\end{itemize}
		
		\bigskip
		
		Os experimentos consistem em:
		\begin{itemize}
			\item 20 repetições de cada cenário.
			\item Código implementado na linguagem de programação Python.
			\item Parâmetro $limit$ definido como $limit=SN*D$ \shortcite{Karaboga2005, Karaboga2008}.
		\end{itemize}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Ambiente de testes}
		
		Para determinação de $d$, foram testados os valores $d=1, 2, \dots, D$, com três tamanhos de população e três quantides de $maxCycle$.
		
		\bigskip
		
		Para determinação de $NP$, $NP=1, 2, \dots, \floor{maxCycle}$.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Algoritmo Final}
		
		\begin{algorithm}[H]
			\KwData{$f$, $MCF$}
			\KwResult{$M_s$}
			\Begin{
				$[d, NP, maxCycle, SN, limit] \leftarrow$ calcParams($MCF$)\;
				inicialização()\;
				iteração $\leftarrow 1$\;
				
				\Repeat{iteração $\le$ $maxCycle$}{
					faseDasEmpregadas()\;
					faseDasObservadoras()\;
					faseDasEscoteiras()\;
					memorizarSolução()\;
					iteração $\leftarrow$ iteração + 1\;
				}
			}
		\end{algorithm}
		
	\end{frame}
	
	\section{Resultados e Análise}
	
	\begin{frame}
		\frametitle{Resultados e Análise}
		
		As repetições dos experimentos geraram um resultado médio $f_i(X)$.
		
		\bigskip
		
		Seja $f_i(X^*)$ o ótimo global de $f_i$. Se $f_i(X)-f_i(X^*)<10^{-30}$, considera-se que o algoritmo atingiu o mínimo global.
		
		\bigskip
		
		Dois resultados médios com diferença inferior a $10^{-30}$ serão considerados iguais.
		
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Número de Dimensões na Equação de Busca}
		
		Foram analisadas 27 configurações de cenário:
		\begin{itemize}
			\item $maxCycle$ = 1000, 2000 e 5000.
			\item $NP$ = 20, 50 e 80.
			\item $D$ = 10, 20 e 30.
		\end{itemize}
		
		\bigskip
		
		freq($d$): frequência com que um valor de $d$ forneceu o melhor resultado médio para as 12 funções.
		
		\smallskip
		
		ex:
		
		\begin{figure}
			\includegraphics[width=0.4\textwidth]{images/freq_20pop_10d_500iter.pdf}
		\end{figure}
		
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Número de Dimensões na Equação de Busca}
		
		\begin{figure}[!h]
			\centering
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20pop_10d_500iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20pop_10d_2000iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20pop_10d_5000iter.pdf}}
			
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20pop_20d_500iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20pop_20d_2000iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20pop_20d_5000iter.pdf}}
			
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20pop_30d_500iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20pop_30d_2000iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20pop_30d_5000iter.pdf}}
		\end{figure}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Número de Dimensões na Equação de Busca}
		
		\begin{figure}[!h]
			\centering
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_50pop_10d_500iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_50pop_10d_2000iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_50pop_10d_5000iter.pdf}}
			
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_50pop_20d_500iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_50pop_20d_2000iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_50pop_20d_5000iter.pdf}}
			
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_50pop_30d_500iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_50pop_30d_2000iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_50pop_30d_5000iter.pdf}}	
		\end{figure}
		
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Número de Dimensões na Equação de Busca}
		
		\begin{figure}[!h]
			\centering
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_80pop_10d_500iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_80pop_10d_2000iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_80pop_10d_5000iter.pdf}}
			
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_80pop_20d_500iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_80pop_20d_2000iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_80pop_20d_5000iter.pdf}}
			
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20pop_30d_500iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20pop_30d_2000iter.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20pop_30d_5000iter.pdf}}
		\end{figure}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Número de Dimensões na Equação de Busca}
		
		Algumas funções são melhor otimizadas com $d$ baixo, e outras, com $d$ alto.
		
		\bigskip
		
		Se o operador optar por executar o ABC mais de uma vez, uma boa estratégia seria escolher $d=1$ e $d=D$ em duas execuções distintas.
		
		\bigskip
		
		Caso contrário, uma estratégia alternativa faz-se necessária: escolher o $d$ que retorne o menor valor em \textbf{média}.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Número de Dimensões na Equação de Busca}
		
		Escolha de $d$:
		
		\begin{itemize}
			\item Resultados foram normalizados.
			\begin{equation}
			\mathbf{z}_i = \frac{\mathbf{x}_i - min(\mathbf{x})}{max(\mathbf{x})-min(\mathbf{x})},
			\end{equation}
			\item A média dos resultados para $d=1,2,\dots,D$ foi calculada.
		\end{itemize}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Número de Dimensões na Equação de Busca}
		
		\begin{figure}[!h]
			\centering
			\setcounter{subfigure}{0}
			\subfloat[$D=10$]{\label{fig:mean_20pop_10d}\includegraphics[width=0.33\textwidth]{images/mean_20pop_10d.pdf}}
			\subfloat[$D=20$]{\label{fig:mean_20pop_20d}\includegraphics[width=0.33\textwidth]{images/mean_20pop_20d.pdf}}
			\subfloat[$D=30$]{\label{fig:mean_20pop_30d}\includegraphics[width=0.33\textwidth]{images/mean_20pop_30d.pdf}}
		\end{figure}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Número de Dimensões na Equação de Busca}
		
		\begin{figure}[!h]
			\centering
			\setcounter{subfigure}{0}
			\subfloat[$NP=20$]{\label{fig:mean_20pop_s}\includegraphics[width=0.33\textwidth]{images/mean_20pop.pdf}}
			\subfloat[$NP=50$]{\label{fig:mean_50pop_s}\includegraphics[width=0.33\textwidth]{images/mean_50pop.pdf}}
			\subfloat[$NP=80$]{\label{fig:mean_80pop_s}\includegraphics[width=0.33\textwidth]{images/mean_80pop.pdf}}
		\end{figure}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Número de Dimensões na Equação de Busca}
		
		Determinando $d$ em função do número de consultas à função objetivo (MCF):
		\begin{enumerate}
			\item Estipular MCF.
			\item Para cada $NP$, iterar até que MCF seja alcançado.
			\item Realizar o procedimento para $d=1,2,\dots,D$.
			\item Normalizar e calcular a média.
			\item Escolher $d$ que resulte no menor valor médio.
		\end{enumerate}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Número de Dimensões na Equação de Busca}
		
		\begin{figure}[!h]
			\centering
			\includegraphics[height=0.8\textheight]{images/mcf.pdf}
		\end{figure}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Tamanho da população}
		
		Foram analisadas 27 configurações de cenário:
		\begin{itemize}
			\item $D=10,20,30$.
			\item MCF = 10000, 25000, 50000, 75000, 100000, 125000, 150000, 175000 e 200000.
		\end{itemize}
		
		freq($NP$): frequência com que um valor de $NP$ forneceu o melhor resultado médio para as 12 funções.
		
		\smallskip
		
		ex:
		
		\begin{figure}
			\includegraphics[width=0.4\textwidth]{images/freq_10d_10000mcf.pdf}
		\end{figure}
	\end{frame}
	
	\begin{frame}
		\frametitle{Tamanho da população}
		
		\begin{figure}[!h]
			\centering
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_10d_10000mcf.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20d_10000mcf.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_30d_10000mcf.pdf}}
			
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_10d_75000mcf.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20d_75000mcf.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_30d_75000mcf.pdf}}
			
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_10d_150000mcf.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_20d_150000mcf.pdf}}
			\subfloat{\includegraphics[trim={0 1cm 0 0.4cm},clip, width=0.33\textwidth]{images/freq_30d_150000mcf.pdf}}
		\end{figure}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Tamanho da população}
		
		Seguindo a mesma estratégia utilizada para $d$:
		
		\begin{figure}[!h]
			\centering
			\setcounter{subfigure}{0}
			\subfloat[MCF $=10000$]{\label{fig:mean_10000mcf}\includegraphics[width=0.33\textwidth]{images/mean_10000mcf.pdf}}
			\subfloat[MCF $=75000$]{\label{fig:mean_75000mcf}\includegraphics[width=0.33\textwidth]{images/mean_75000mcf.pdf}}
			\subfloat[MCF $=150000$]{\label{fig:mean_150000mcf}\includegraphics[width=0.33\textwidth]{images/mean_150000mcf.pdf}}
		\end{figure}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Tamanho da população}
		
		\begin{figure}
			\centering
			\includegraphics[height=0.8\textheight]{images/mean_mcf_fit.pdf}
		\end{figure}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Calcula Parâmetros}
		
		\begin{algorithm}[H]
			\KwData{$MCF$}
			\KwResult{$d, NP, maxCycle, SN, limit$}
			\Begin{
				$d \leftarrow$ mínimo$(\floor{3.77^{-5} \times MCF + 1}, D)$\;
				$NP \leftarrow$ mínimo$(\floor{1.08^{-4} \times MCF + 22.42}, \sqrt{MCF})$\;
				$maxCycle \leftarrow$ $\floor{MCF/(NP+1)}$\;
				$SN \leftarrow \floor{NP/2}$\;
				$limit \leftarrow SN \times D$\;
			}
		\end{algorithm}
		
	\end{frame}
	
	\section{Conclusão}
	
	\begin{frame}
		\frametitle{Conclusão}
		
		Este trabalho desenvolveu um método para determinação automática de $d$ e $NP$, baseando-se no número máximo de consultas à função objetivo.
		
		\bigskip
		
		Visto que não é possível a determinação de parâmetros ótimos para todas as classes de problemas, este estudo buscou identificar regras que fornecessem bons resultados médios para problemas com variadas características.
		
		\bigskip
		
		Os resultados mostraram que quanto mais processamento é cedido ao algoritmo, a tendência é optar-se por um maior valor para $d$. A tendência é similar para $NP$.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Conclusão}
		
		A associação entre análises previamente estabelecidas na literatura e as análises aqui concebidas deram origem a uma versão simplificada do algoritmo ABC, onde apenas o número máximo de consultas à função objetivo precisa ser estipulado para utilização da técnica.
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Trabalhos Futuros}
		
		Trabalhos futuros:
		\begin{itemize}
			\item Continuar o estudo teórico com relação ao parâmetro $d$.
			\begin{itemize}
				\item Verificar a diferença nos resultados usando $\phi_1, \phi_2, \dots, \phi_d$ iguais ou não.
			\end{itemize}
			\item Determinar se o ABC possui \textit{bias} na origem.
			\item Realizar estudos mais profundos no cálculo da função \textit{fitness}.
		\end{itemize}
		
	\end{frame}
	
	\setbeamertemplate{logo}{}
	
	\begin{frame}
		\frametitle{Artigos}
		
		\textit{GOMES, W.; SANTOS, R.; SALES, C. An improved artificial bee colony algorithm with diversity control. In: 7th Brazilian Conference on Intelligent Systems (BRACIS). São Paulo, Brasil: IEEE, 2018.}
		
	\end{frame}
	
	\begin{frame}
		\frametitle{Agradecimentos}
		
		\begin{figure}
			\centering
			\subfloat{\includegraphics[width=0.2\textwidth]{images/cnpq-logo-7.png}}
			\hspace{0.3\textwidth}
			\subfloat{\includegraphics[width=0.1\textwidth]{images/ufpa.png}}
		\end{figure}
		
		\bigskip
		
		\begin{center}
			\Large
			Obrigado!
		\end{center}
		
	\end{frame}
	
	%------------------------------------------------------------
	%- Bibliography
	\begin{frame}[allowframebreaks]
		\frametitle{Bibliografia}
		\bibliographystyle{apacite}
		\bibliography{references}
	\end{frame}
	
\end{document}