import numpy as np
import matplotlib.pyplot as plt
from problems import *
from artificialBeeColony import ArtificialBeeColony as ABC
from artificialBeeColony_new import ArtificialBeeColony as ABCD


def sigmoidx(x):
	return 1 / (1 + np.exp(-x*10+5))
	

nRuns = 3
popSize = 30
iterations = 500
dimensionality = 30
limit = (popSize//2)*dimensionality

evalFunc, boundaries, name = optFunction(1)

convergence_curve1 = np.zeros(iterations)
convergence_curve2 = np.zeros(iterations)
rejection_rate = np.zeros(iterations)
diversity = np.zeros(iterations)

for run in range(nRuns):
	print("Run "+ str(run))
	abc  = ABC(popSize, limit, evalFunc, dimensionality, boundaries)
	abcd = ABCD(popSize, limit, evalFunc, dimensionality, boundaries)
	
	
	for i in range(iterations):
		abc.iterate()
		abcd.iterate()
		convergence_curve1[i] += abc.globalMin/nRuns
		convergence_curve2[i] += abcd.globalMin/nRuns
		rejection_rate[i] += (float(abcd.rejections)/popSize)/nRuns
		diversity[i] += abcd.popDiversity()/nRuns
		
		# Testando inclusão de gradiente. Parece que não dá para melhorar a velocidade sem aumentar a computação
		abcd.prob = 0 #sigmoidx(float(i)/iterations)

print(name)
print('ABC global Minimum:', abc.globalMin)
print('ABCD global Minimum:', abcd.globalMin)


plt.figure(figsize=(12,6))
plt.subplot(121)
plt.plot(convergence_curve1)
plt.plot(convergence_curve2)
plt.legend(['ABC','ABCD'])

plt.subplot(122)
plt.plot(rejection_rate)
plt.plot(diversity)
plt.plot()
plt.legend(['Rejection Rate','Diversity'])

plt.show()
