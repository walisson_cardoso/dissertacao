\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Introdu\IeC {\c c}\IeC {\~a}o}{3}{0}{1}
\beamer@sectionintoc {2}{Trabalhos Relacionados}{7}{0}{2}
\beamer@sectionintoc {3}{Col\IeC {\^o}nia Artificial de Abelhas}{11}{0}{3}
\beamer@sectionintoc {4}{Equa\IeC {\c c}\IeC {\~a}o de Busca}{17}{0}{4}
\beamer@sectionintoc {5}{Experimentos e An\IeC {\'a}lises}{25}{0}{5}
\beamer@subsectionintoc {5}{1}{Fun\IeC {\c c}\IeC {\~o}es de Teste}{27}{0}{5}
\beamer@subsectionintoc {5}{2}{Metodologia}{29}{0}{5}
\beamer@subsectionintoc {5}{3}{Resultados}{30}{0}{5}
\beamer@sectionintoc {6}{Conclus\IeC {\~a}o}{33}{0}{6}
