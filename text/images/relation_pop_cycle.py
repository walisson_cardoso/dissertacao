import numpy as np
import matplotlib.pyplot as plt

f_max = 20000
pop = range(2,int(np.sqrt(f_max)),2)
iter = []

for p in pop:
	iter.append(f_max/p)

plt.plot(pop,iter, linewidth='3')
plt.ylabel('$maxCycle$',fontsize=15)
plt.xlabel('$NP$',fontsize=15)
plt.grid('on')
plt.show()
