# -*- coding: utf-8 -*-

# This script loads a bunch of files resulting from several optimizations and plots them
# This function is in accordance with the verify_dimensionality script

# Imports
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from problems import optFunction
from scipy.optimize import curve_fit

def func(x, a, b):
    return a*x + b

# How many functions, dimensions of the problem, population size NP, maxCycle and repetition for configutation
nFuncs = 12
repetitions = 10

mcfvals = np.array([10000, 25000, 50000, 75000, 100000, 125000, 150000, 175000, 200000])
mcfbest = np.zeros(len(mcfvals))

allx = []
ally = []

colors = ['bo', 'ro', 'go']
for color,nDim in enumerate([10, 20, 30]):
    for i,mcf in enumerate(mcfvals):
        folderName = '/home/gomes/Documents/Mestrado/Back up Mestrado/mcf/'
        filepath = folderName + str(nDim) + 'd_' + str(mcf) + 'mcf.txt'
        data = np.loadtxt(filepath, delimiter=',')
        
        popSize = []
        for p in data[0]:
            popSize.append(int(p))
        nPop = len(popSize)
        avgMean = np.zeros(nPop)
        avgStd = np.zeros(nPop)
        
        for f in range(nFuncs):
            funcCal, limits, funcName = optFunction(f)
            
            data_f = data[f*repetitions+1:(f+1)*repetitions+1]
            
            meanVal = np.mean(data_f, axis=0)
            stdVal = np.std(data_f, axis=0)
            
            if meanVal.max() - meanVal.min() != 0.0:
                avgMean += (meanVal - meanVal.min())/(meanVal.max() - meanVal.min())
                avgStd += (stdVal - stdVal.min())/(stdVal.max() - stdVal.min())
                            
        avgMean /= nFuncs
        avgStd  /= nFuncs
    
        mcfbest[i] = popSize[np.argmin(avgMean)]
    
    label = '$NP=$' + str(nDim)
    plt.plot(mcfvals, mcfbest, colors[color], label = label)
    print(mcfbest)
    
    for v in mcfvals:
        allx.append(v)
    for v in mcfbest:
        ally.append(v)

mcfvals = np.array(allx)
mcfbest = np.array(ally)

popt, pcov = curve_fit(func, mcfvals, mcfbest, maxfev=8000)
plt.plot(mcfvals, func(mcfvals, *popt), 'r--', linewidth=2, label = 'Curva ajustada')
print(popt)

plt.legend(loc = 'upper left')
plt.ylim([0, 80])
plt.xlabel('MCF')
plt.ylabel('$M($MCF$)=NP$')
plt.xticks([10000, 50000, 100000, 150000, 200000], ['10000', '50000', '100000', '150000', '200000'])
plt.locator_params(axis='x', nbins=5)
    
name = 'mean_mcf_fit.pdf'
plt.show(name)
