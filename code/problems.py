import numpy as np
    

def ackley(sol):
	D = len(sol)
	a = 20
	b = 0.2
	c = 2*np.pi
	d = D
	
	sum1 = 0.0
	sum2 = 0.0
	for i in range(d):
		xi = sol[i]
		sum1 = sum1 + xi**2
		sum2 = sum2 + np.cos(c*xi)

	term1 = -a*np.exp(-b*np.sqrt(sum1/d))
	term2 = -np.exp(sum2/d)

	return term1 + term2 + a + np.exp(1)

def alpinen1(sol):
	D = len(sol)
	top = 0.0
	for i in range(D):
		xi = sol[i]
		top += abs(xi*np.sin(xi)+0.1*xi)
	
	return top;
	

def griewank(sol):
	D = len(sol)
	top  = 0.0
	prod = 1.0

	for i in range(D):
		xi = sol[i]
		top = top + (xi**2)/4000.0
		prod = prod * np.cos(xi/np.sqrt(i+1))
	
	return top - prod + 1


def levy(sol):
	D = len(sol)
	w = np.zeros(D)
	for i in range(D):
		w[i] = 1.0 + (sol[i]-1.0)/4.0

	term1 = (np.sin(np.pi*w[0]))**2
	term3 = ((w[D-1]-1)**2) * (1+(np.sin(2*np.pi*w[D-1]))**2)

	s = 0
	for i in range(D-1):
		wi = w[i]
		s += (wi-1)**2 * (1+10*(np.sin(np.pi*wi+1))**2)

	return term1 + s + term3



def michalewicz(sol):
	D = len(sol)
	m = 10.0
	top = 0.0

	for i in range(D):
		xi = sol[i]
		n = np.sin(xi) * ((np.sin((i+1)*(xi**2)/np.pi)) ** (2*m))
		top = top + n

	return -top


def rastrigin(sol):
	D = len(sol)
        top = 0.0
        for i in range(D):
            xi  = sol[i]
            top = top + ((xi**2) - 10*np.cos(2*np.pi*xi))

        return 10*D + top


def rosenbrock(sol):
	D = len(sol)
	top = 0.0
	for i in range(D-1):
		xi = sol[i]
		xnext = sol[i+1]
		n = 100*((xnext-xi**2)**2) + (xi-1)**2
		top = top + n
			
	return top


def shubert3(sol):
    D = len(sol);
    scores = 0.0;
    for i in range(D):
    	for j in range(1,6):
    		scores += j*np.sin((j + 1)*sol[i]+j)
    		
    return scores


def schwefel(sol):
	D = len(sol)
	top = 0.0
	for i in range(D):
		xi = sol[i]
		top = top + xi*np.sin(np.sqrt(abs(xi)))

	return 418.98288727243379*D - top


def styblinskiTank(sol):
	D = len(sol)
	top = 0
	for i in range(D):
		top += sol[i]**4 - 16*(sol[i]**2) + 5*sol[i]
	return top*0.5


def sumOfSquare(sol):
	D = len(sol)
	top = 0.0
	for i in range(D):
		xi = sol[i]
		top = top + (i+1)*(xi**2)
		
	return top


def zakharov(sol):
	D = len(sol)
	sum1 = 0.0
	sum2 = 0.0

	for i in range(D):
		xi = sol[i]
		sum1 += xi**2
		sum2 += 0.5*(i+1)*xi

	return sum1 + sum2**2 + sum2**4;



		

def optFunction(index):

	name = ["Ackley",
		"Alpine N.1",
		"Griewank",
		"Levy",
		"Michalewicz",
		"Rastrigin",
		"Rosenbrock",
		"Schwefel",
		"Shubert 3",
		"Styblinski-Tank",
		"Sum of Squares",
		"Zakharov"]

	funcs = [ackley,
		alpinen1,
		griewank,
		levy,
		michalewicz,
		rastrigin,
		rosenbrock,
		schwefel,
		shubert3,
		styblinskiTank,
		sumOfSquare,
		zakharov]
		
	limits = [[-32.768, 32.768],
		[-10.0, 10.0],
		[-600.0, 600.0],
		[-10.0,10.0],
		[-0.0, np.pi],
		[-5.12, 5.12],
		[-5.0, 10.0],
		[-500.0, 500.0],
		[-10.0,10.0],
		[-50.0,50.0],
		[-100.0, 100.0],
		[-5.0,10.0]]
	
	return (funcs[index], limits[index], name[index])
