# -*- coding: utf-8 -*-

# This script loads a bunch of files resulting from several optimizations and plots them
# This function is in accordance with the verify_dimensionality script

# Imports
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from problems import optFunction

# How many functions, dimensions of the problem, population size NP, maxCycle and repetition for configutation
nFuncs = 12
repetitions = 10

for nDim in [10, 20, 30]:
    for mcf in [10000, 75000, 150000]:
        fig, axes = plt.subplots(nFuncs,1,figsize=(4,17))
        plt.subplots_adjust(hspace=0.05, bottom = 0.01, top = 0.99)

        folderName = '/home/gomes/Documents/Mestrado/Back up Mestrado//mcf/'
        filepath = folderName + str(nDim) + 'd_' + str(mcf) + 'mcf.txt'
        data = np.loadtxt(filepath, delimiter=',')
        
        popSize = []
        for p in data[0]:
            popSize.append(int(p))
        nPop = len(popSize)
        best_p = np.zeros(nPop)
        
        for f in range(nFuncs):
            funcCal, limits, funcName = optFunction(f)
            
            data_f = data[f*repetitions+1:(f+1)*repetitions+1]
            
            meanVal = np.mean(data_f, axis=0)
            stdVal = np.std(data_f, axis=0)

            r2 = [meanVal.min(), meanVal.max()]
            ax = axes[f]
            ax.grid(which='major', color='gray', linestyle='dotted')
            ax.set_yticks(r2)

            smin = np.format_float_scientific(r2[0],precision=2,trim='k')
            smax = np.format_float_scientific(r2[1],precision=2,trim='k')
            ax.set_yticklabels([smin,smax])

            label = '$f_{' + str(f+1) + '}(X)$'
            ax.set_ylabel(label)
            ax.yaxis.tick_right()
            ax.tick_params(axis='x', colors='white')

            ax.plot(popSize, meanVal, 'black')
            for i,val in enumerate(meanVal):
                if abs(val - r2[0]) <= 1e-30:
                    ax.plot(popSize[i], val, 'ro')
                    best_p[i] += 1


        ax = axes[-1]
        ax.tick_params(axis='x', colors='black')
        plt.locator_params(axis='x', nbins=5)
        plt.xlabel('$NP$', fontsize = 12)
        plt.tight_layout()
        
        name = str(nDim) + 'd_' + str(mcf) + 'mcf.pdf'
        plt.savefig(name)
		
		
        #Show the ds with best results
        
        plt.figure(figsize=(5,2.5))
        plt.grid(which='major', color='gray', linestyle='dotted')
        plt.yticks([best_p.min(), best_p.max()])
        plt.xlabel('$NP$', fontsize = 13)
        plt.ylabel('freq($NP$)', fontsize = 13)
        
        #ax.yaxis.tick_right()
        plt.plot(popSize, best_p)
        plt.locator_params(axis='x', nbins=5)
        plt.tight_layout()
        
        name = 'freq_' + str(nDim) + 'd_' + str(mcf) + 'mcf.pdf'
        plt.savefig(name)
        
        print('saved')
