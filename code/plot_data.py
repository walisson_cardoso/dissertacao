# -*- coding: utf-8 -*-

# This script loads a bunch of files resulting from several optimizations and plots them
# This function is in accordance with the verify_dimensionality script

# Imports
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from problems import optFunction

# How many functions, dimensions of the problem, population size NP, maxCycle and repetition for configutation
nFuncs = 12
repetitions = 10

for popSize in [20, 50, 80]:
	for nDim in [10, 20, 30]:
		for nIter in [500, 2000, 5000]:
			best_d = np.zeros(nDim)
			fig, axes = plt.subplots(nFuncs,1,figsize=(4,17))
			plt.subplots_adjust(hspace=0.05, bottom = 0.01, top = 0.99)

			for p in range(nFuncs):
				funcCal, limits, funcName = optFunction(p)
				folderName = '/home/gomes/Documents/Mestrado/Back up Mestrado/' + str(popSize) + 'ind/'
				fileName = folderName + funcName + '_' + str(nDim) + 'd_' + str(popSize) + 'ind.txt'
				data = np.loadtxt(fileName,delimiter=',')
	
				meanVal = np.zeros(nDim)
				stdVal = np.zeros(nDim)
				for i in range(nDim):
					data_d = data[i*repetitions:(i+1)*repetitions]
		
					values = np.zeros(repetitions)
					for iter,d in enumerate(data_d):
						values[iter] = d[nIter-1]
		
					mean = values.mean()
					std = values.std()
					meanVal[i] = mean
					stdVal[i] = std
	
				r1 = [meanVal.min(), meanVal.max()]
	
				#if r1[0] <= 0:                         # For log visualization
				#	meanVal = meanVal+abs(r1[0])+1e-30
	
				r2 = [meanVal.min(), meanVal.max()]
				ax = axes[p]
				ax.grid(which='major', color='gray', linestyle='dotted')
				ax.set_yticks(r2)
	
				smin = np.format_float_scientific(r1[0],precision=2,trim='k')
				smax = np.format_float_scientific(r1[1],precision=2,trim='k')
				ax.set_yticklabels([smin,smax])
	
				label = '$f_{' + str(p+1) + '}(X)$'
				ax.set_ylabel(label)
				ax.yaxis.tick_right()
				ax.set_xticks(np.arange(nDim/10,nDim+1,nDim/10))
				ax.tick_params(axis='x', colors='white')
	
				x = range(1,nDim+1)
				ax.plot(x,meanVal,'black')
				#ax.fill_between(x, meanVal+stdVal,meanVal-stdVal,facecolor='blue',alpha=0.3)
				for i,val in enumerate(meanVal):
					if abs(val - r2[0]) <= 1e-30:
						ax.plot(i+1, val, 'ro')
						best_d[i] += 1
						
			ax = axes[-1]
			ax.set_xticks(np.arange(nDim/10,nDim+1,nDim/10))
			ax.tick_params(axis='x', colors='black')
			ax.set_xlabel('$d$', fontsize=12)
			plt.tight_layout()
			
			name = str(popSize) + 'pop_' + str(nDim) + 'd_' + str(nIter) + 'iter.pdf'
			plt.savefig(name)
			
			
			#Show the ds with best results
			
			plt.figure(figsize=(5,2.5))
			plt.grid(which='major', color='gray', linestyle='dotted')
			plt.yticks([best_d.min(), best_d.max()])
			plt.xlabel('$d$', fontsize=13)
			plt.ylabel('freq($d$)', fontsize=13)

			#ax.yaxis.tick_right()
			plt.xticks(np.arange(nDim/10,nDim+1,nDim/10))
			plt.plot(range(1,nDim+1), best_d)
			plt.tight_layout()
			
			name = 'freq_' + str(popSize) + 'pop_' + str(nDim) + 'd_' + str(nIter) + 'iter.pdf'
			plt.savefig(name)
			
			plt.close()
			
			print('saved')
